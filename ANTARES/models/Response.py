import nifty6 as ift
from nifty6.operators.operator import _OpChain
import numpy as np
from ANTARES.models import LightSources as LS
import time

###############################################################################
# AngularAcceptance
###############################################################################

def ang_accept_single(ct):
    """
    Fitted angular acceptance of ANTARES optical modules.
    Provided by ANTARES collaboration.

    :param ct: cos(theta)
    :return: angular acceptance, diff of angular acceptance
    """

    a = [0.3265, 0.6144, -0.0343, -0.0641, 0.2988, -0.1422]


    if ct < -0.65072900712109106509536326533479565423957719:
        aa = 0.0
    else:
     aa = a[0] + \
          a[1] * ct + \
          a[2] * ct ** 2 + \
          a[3] * ct ** 3 + \
          a[4] * ct ** 4 + \
          a[5] * ct ** 5


    return aa

def ang_accept(ct):
    """
    Fitted angular acceptance of ANTARES optical modules.
    Provided by ANTARES collaboration.

    :param ct: cos(theta)
    :return: angular acceptance, diff of angular acceptance
    """

    a = [0.3265, 0.6144, -0.0343, -0.0641, 0.2988, -0.1422]

    aa = a[0] + \
         a[1] * ct + \
         a[2] * ct ** 2 + \
         a[3] * ct ** 3 + \
         a[4] * ct ** 4 + \
         a[5] * ct ** 5

    aa_prime = a[1] + \
               2 * a[2] * ct ** 1 + \
               3 * a[3] * ct ** 2 + \
               4 * a[4] * ct ** 3 + \
               5 * a[5] * ct ** 4

    aa[ct < -0.65072900712109106509536326533479565423957719] = 0.0
    aa_prime[ct < -0.65072900712109106509536326533479565423957719] = 0.0

    return aa, aa_prime


###############################################################################
# OM Position depended Operator
###############################################################################


class OMPositionOperator(ift.Operator):
    """
    IFT Operator for ANTARES Optical Modules.
    Represents the response of the optical module, depending on:
    - position
    - view direction
    - size of surface
    - angular acceptance
    """

    def __init__(self, om_pos, om_dir, om_r, om_id, target):
        """
        Initialize operator by passing optical module attributes.

        :param om_pos: position of om
        :param om_dir: direction of om
        :param om_r: radius of om surface
        :param om_id:  om id
        :param target: target domain
        """

        self._pos_key = ['x', 'y', 'z']
        self._l_att_key = 'l_att_factor'

        # Initialize domains
        self._target = target
        self._domain = ift.MultiDomain.make(
            {'x': self._target,
             'y': self._target,
             'z': self._target,
             'l_att_factor': self._target})

        # attributes
        self.id = om_id
        self.om_pos = om_pos
        self.om_dir = om_dir
        self.om_r = om_r

        # For debugging
        self.ct = None
        self.water_abs = None
        self.r_vec = None
        self.r = None
        self.AA = None
        self.AA_prime = None
        self.d = None

    def __repr__(self):
        s = 'OMPositionOperator'
        return s

    def apply(self, x):
        """
        Applying response BioSource signal and water absoption (x,y,z, l_att).

        :param x: (x, y, z, l_att)
        :return: Field
        """
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        xval = x
        if lin:
            xval = x.val

        # Seperate signal
        bo_pos_np = np.array([xval[self._pos_key[0]].val,
                              xval[self._pos_key[1]].val,
                              xval[self._pos_key[2]].val]).T
        l_att = xval[self._l_att_key].val

        # Numpy arrays
        om_pos_np = np.array(self.om_pos)
        om_dir_np = np.array(self.om_dir)

        # Vector between source and om
        r_vec_np = bo_pos_np - om_pos_np
        # + np.array([1e-12, 1e-12, 1e-12])

        # Distance
        r_np = np.sqrt(np.sum(r_vec_np ** 2, axis=1))

        # Angular acceptance
        ct = np.dot(om_dir_np, r_vec_np.T / r_np)
        aa, aa_prime = ang_accept(ct)

        # Water absoprtion
        #water_abs = np.exp(-1/l_att * r_np)
        water_abs = l_att ** (r_np)

        # OM geometry factor
        geom_factor = self.om_r**2 / (4 * r_np ** 2)

        # loss
        loss = geom_factor * \
               water_abs * \
               aa

        # Derivatives
        factor = -geom_factor/r_np * water_abs

        #dx = factor * r_vec_np[:, 0] * (
        #        aa * 2 / r_np +
        #        aa / l_att +
        #        aa_prime * (ct / r_np - om_dir_np[0] / r_vec_np[:, 0])
        #)
        #dy = factor * r_vec_np[:, 1] * (
        #        aa * 2 / r_np +
        #        aa / l_att +
        #        aa_prime * (ct / r_np - om_dir_np[1] / r_vec_np[:, 1])
        #)
        #dz = factor * r_vec_np[:, 2] * (
        #        aa * 2 / r_np +
        #        aa / l_att +
        #        aa_prime * (ct / r_np - om_dir_np[2] / r_vec_np[:, 2])
        #)

        #dl_att = geom_factor * r_np * \
        #       water_abs * \
        #       aa / l_att**2

        dx = factor * r_vec_np[:, 0] * (
                aa * 2 / r_np -
                aa * np.log(l_att) +
                aa_prime * (ct / r_np - om_dir_np[0] / r_vec_np[:, 0])
        )
        dy = factor * r_vec_np[:, 1] * (
                aa * 2 / r_np -
                aa * np.log(l_att) +
                aa_prime * (ct / r_np - om_dir_np[1] / r_vec_np[:, 1])
        )
        dz = factor * r_vec_np[:, 2] * (
                aa * 2 / r_np -
                aa * np.log(l_att) +
                aa_prime * (ct / r_np - om_dir_np[2] / r_vec_np[:, 2])
        )

        dl_att = geom_factor * r_np * \
               l_att**(r_np-1) * \
               aa

        # Debugging
        self.ct = ct
        self.r = r_np
        self.r_vec = r_vec_np
        self.water_abs = water_abs
        self.AA = aa
        self.AA_prime = aa_prime
        self.d = (dx, dy, dz)

        if not lin:
            return ift.Field(self._target, loss)

        loss = ift.Field(self._target, loss)
        
        dx = ift.makeOp(ift.Field(self._target, dx)).ducktape(self._pos_key[0])
        dy = ift.makeOp(ift.Field(self._target, dy)).ducktape(self._pos_key[1])
        dz = ift.makeOp(ift.Field(self._target, dz)).ducktape(self._pos_key[2])
        dl_att = ift.makeOp(ift.Field(self._target, dl_att)).ducktape(self._l_att_key)

        

        return x.new(loss, (dx + dy + dz + dl_att)(x.jac))


###############################################################################
# OM Class
###############################################################################

class Response:
    """
    Response of optical module.
    """

    def __init__(self, response_id, mask, domain):
        """
        Response class is initialized with mask, id and domain.

        :param response_id: ID
        :param mask: data mask
        :param domain: data domain
        """
        start_time = time.time()

        # Attributes
        self.time_domain = domain
        self.id = response_id

        # Operators
        self.mask = self._init_mask_op(mask)

        self.init_time = time.time() - start_time
        print("Initializing\tResponse \tComputation time: " + str(
            self.init_time * 1000) + str(
            " ms"))

    def __repr__(self):
        s = 'Response%s' % str(self.id)
        return s

    def __call__(self, om, sources):
        """
        Calling response class overwrite inherent attributes as:
        - response
        - input
        - masked response
        - signal

        :param om: OM class
        :param sources: sources list
        :return:
        """

        # Operator
        self.OMPosOp = OMPositionOperator(om.pos, om.dir, om.r, om.id, target=self.time_domain)

        # Response
        self.response, self.responses, self.input, self.inputs = self._init_responses(om, sources)
        self.masked_response = self.mask(self.response)
        self.signal = self._init_signal_field()

    def _init_responses(self, om, sources):

        responses = {}
        inputs = {}

        expander = ift.VdotOperator(ift.full(self.time_domain, 1))

        for source in sources:
            if isinstance(source, LS.NoiseSource):
                inputs[source.id] = source.lumin
            elif isinstance(source, LS.BioSource):

                # TODO: better nifty way
                tmp_domain = ift.MultiDomain.make(
                    {'l_att_factor': self.time_domain,
                     'x': self.time_domain,
                     'y': self.time_domain,
                     'z': self.time_domain,})

                tmp = ift.FieldAdapter(tmp_domain, 'x')(source.x) + \
                      ift.FieldAdapter(tmp_domain, 'y')(source.y) + \
                      ift.FieldAdapter(tmp_domain, 'z')(source.z) + \
                      ift.FieldAdapter(tmp_domain, 'l_att_factor')(expander.adjoint(om.l_att_factor))

                inputs[source.id] = expander.adjoint(om.aniso_eff) * source.lumin * self.OMPosOp(tmp)

        input = ift.utilities.my_sum(list(inputs.values()))
        response = expander.adjoint(om.eff) * input

        return response, responses, input, inputs

    def _init_mask_op(self, mask):

        # Masking operator to model that parts of the field have not been observed
        mask = ift.Field.from_raw(self.time_domain, mask)
        mask = ift.MaskOperator(mask)

        return mask

    def _init_signal_field(self):
        """
        Joint of different signals.

        :return: signal as MultiField
        """

        self.signal_time_domain = ift.MultiDomain.make(
            {'response': self.time_domain,
             'mask response': self.mask.target,
             'input': self.time_domain})

        signal = ift.FieldAdapter(self.signal_time_domain, 'input')(self.input) + \
                 ift.FieldAdapter(self.signal_time_domain, 'mask response')(self.masked_response) + \
                 ift.FieldAdapter(self.signal_time_domain, 'response')(self.response)

        return signal
