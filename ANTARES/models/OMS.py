import nifty6 as ift
from nifty6.operators.operator import _OpChain
from ANTARES.utils import nifty as nifty_utils
import numpy as np
from ANTARES.models import LightSources as LS
import time

###############################################################################
# OM Class
#
# describing a single optical module of the ANTARES detector 
# and its surroundings
###############################################################################

class OM:

    def __init__(self, om_pos, om_dir, om_id, storey_id,
                 r=0.118,
                 eff_init=[0.8, 0, 1, 0],
                 aniso_eff_init=[1, 0, 0, 0],
                 l_att_init=[60, 0, 1, 0]):
        """
        Initialize optical module with 

        :param om_pos: position
        :param om_dir: focus direction
        :param om_id: id
        :param r: radius of effective surface
        :param eff_init: efficiency [mean, std mean, std, std std]
        :param l_att_init: water absorpation [mean, std mean, std, std std]
        :param target:
        """
        start_time = time.time()

        self.id = om_id
        self.storey_id = storey_id
        self.domain = ift.DomainTuple.scalar_domain()
        self.signal_domain = ift.MultiDomain.make(
            {'eff': self.domain,
             'eff_std': self.domain,
             'aniso_eff': self.domain,
             'aniso_eff_std': self.domain,
             'l_att_factor': self.domain,
             'l_att': self.domain,
             'l_att_std': self.domain})

        # Fixed OM attributes
        self.pos = om_pos
        self.dir = om_dir
        self.r = r

        # Trainable OM attributes
        self.eff_init = eff_init
        self.aniso_eff_init = aniso_eff_init
        self.l_att_init = l_att_init
        self.eff, self.eff_std = self._init_eff(eff_init)
        self.aniso_eff, self.aniso_eff_std = self._init_aniso_eff(aniso_eff_init)
        self.l_att_factor, self.l_att, self.l_att_std = self._init_l_att(l_att_init)

        self.signal = self._init_signal_field()

        self.init_time = time.time() - start_time
        print("Initializing\tOM \tComputation time: " + str(
            self.init_time * 1000) + str(" ms"))

    def __repr__(self):
        s = 'OM%s' % str(self.id)
        return s

    def _init_eff(self, init):

        # Helper

        plus_one = ift.Adder(ift.full(self.domain, 1))

        # eff mean
        eff_mean_mean = ift.Adder(ift.full(self.domain, init[0]))
        eff_mean_std = ift.ScalingOperator(self.domain, init[1])
        eff_mean_xi = ift.FieldAdapter(self.domain, 'eff_mean_xi_' + str(self.id))

        eff_mean = eff_mean_mean @ eff_mean_std @ eff_mean_xi

        # eff mean
        if init[2]==0:
            eff_std_mean = ift.Adder(ift.full(self.domain, init[2]))
            eff_std_std = ift.ScalingOperator(self.domain, init[3])
            
            eff_std_xi = ift.FieldAdapter(self.domain, 'eff_std_xi_' + str(self.id))
            eff_std = (eff_std_mean @ eff_std_std @ eff_std_xi)
        else:
            eff_std_mean = ift.Adder(ift.full(self.domain, np.log(init[2]**2/np.sqrt(init[2]**2 + init[3]**2))))
            eff_std_std = ift.ScalingOperator(self.domain, np.sqrt(np.log(1+init[3]**2/init[2]**2)))
            
            eff_std_xi = ift.FieldAdapter(self.domain, 'eff_std_xi_' + str(self.id))
            eff_std = (eff_std_mean @ eff_std_std @ eff_std_xi).exp()

        #eff_std_mean = ift.Adder(ift.full(self.domain, init[2]))
        #eff_std_std = ift.ScalingOperator(self.domain, init[3])
        #eff_std_xi = ift.FieldAdapter(self.domain, 'eff_std_xi_' + str(self.id))

        #eff_std = (plus_one @ (eff_std_mean @ eff_std_std @ eff_std_xi).exp()).log()
        #eff_std = (eff_std_mean @ eff_std_std @ eff_std_xi)

        # eff
        eff_xi = ift.FieldAdapter(self.domain, 'eff_xi_' + str(self.id))

        #eff = expander.adjoint @ (plus_one @ (eff_mean + eff_std * eff_xi).exp()).log()
        eff = (eff_mean + eff_std * eff_xi)

        return eff.clip(a_min=0.01, a_max=1.0), eff_std

    def _init_aniso_eff(self, init):


        # eff mean
        eff_mean = ift.Adder(ift.full(self.domain, init[0]))

        # eff mean
        if init[2]==0 or init[3]==0:
            eff_std_mean = ift.Adder(ift.full(self.domain, init[2]))
            eff_std_std = ift.ScalingOperator(self.domain, init[3])
            
            eff_std_xi = ift.FieldAdapter(self.domain, 'aniso_eff_std_xi_' + str(self.storey_id))
            eff_std = (eff_std_mean @ eff_std_std @ eff_std_xi)
        else:
            eff_std_mean = ift.Adder(ift.full(self.domain, np.log(init[2]**2/np.sqrt(init[2]**2 + init[3]**2))))
            eff_std_std = ift.ScalingOperator(self.domain, np.sqrt(np.log(1+init[3]**2/init[2]**2)))
            
            eff_std_xi = ift.FieldAdapter(self.domain, 'aniso_eff_std_xi_' + str(self.storey_id))
            eff_std = (eff_std_mean @ eff_std_std @ eff_std_xi).exp()


        # eff
        eff_xi = ift.FieldAdapter(self.domain, 'aniso_eff_xi_' + str(self.storey_id))

        #eff = expander.adjoint @ (plus_one @ (eff_mean + eff_std * eff_xi).exp()).log()
        eff = eff_mean @ (eff_std * eff_xi)

        return eff, eff_std


    def _init_l_att(self, init):

        # eff mean
        l_att_mean = ift.Adder(ift.full(self.domain, init[0]))

        # eff mean
        if init[2]==0.0 or init[3]==0.0:
            l_att_std_mean = ift.Adder(ift.full(self.domain, init[2]))
            l_att_std_std = ift.ScalingOperator(self.domain, init[3])
            
            l_att_std_xi = ift.FieldAdapter(self.domain, 'l_att_std_xi_')
            l_att_std = (l_att_std_mean @ l_att_std_std @ l_att_std_xi)
            #l_att_std = l_att_std_mean
        else:
            l_att_std_mean = ift.Adder(ift.full(self.domain, np.log(init[2]**2/np.sqrt(init[2]**2 + init[3]**2))))
            l_att_std_std = ift.ScalingOperator(self.domain, np.sqrt(np.log(1+init[3]**2/init[2]**2)))
            
            l_att_std_xi = ift.FieldAdapter(self.domain, 'l_att_std_xi_')
            l_att_std = (l_att_std_mean @ l_att_std_std @ l_att_std_xi).exp()


        l_att_xi = ift.FieldAdapter(self.domain, 'l_att_xi_')

        l_att =  (l_att_mean @ (l_att_std * l_att_xi))
        l_att_factor =  (-l_att.power(-1)).exp()

        return l_att_factor, l_att, l_att_std


    def _init_signal_field(self):
        """
        Joint of cartesian coordinate spaces.

        :return: signal as MultiField
        """

        signal = ift.FieldAdapter(self.signal_domain, 'eff')(self.eff) + \
                 ift.FieldAdapter(self.signal_domain, 'eff_std')(self.eff_std) + \
                 ift.FieldAdapter(self.signal_domain, 'l_att_factor')(self.l_att_factor) + \
                 ift.FieldAdapter(self.signal_domain, 'l_att')(self.l_att) + \
                 ift.FieldAdapter(self.signal_domain, 'l_att_std')(self.l_att_std) + \
                 ift.FieldAdapter(self.signal_domain, 'aniso_eff')(self.aniso_eff) + \
                 ift.FieldAdapter(self.signal_domain, 'aniso_eff_std')(self.aniso_eff_std)

        return signal
