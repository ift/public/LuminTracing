###############################################################################
# Summary of light source classes
#
# Different classes are build on top of each other
# to describe source emitting photons:
#
#   - basic source including only domain and padding mask
#
#       - noise source including photon emission (not implemented)
#           - const noise source (constant photon emission implemented)
#           - exp nosie source (log normal photon emission implemented)
#
#       - bio source inlcuding only movement (implemented) 
#           and position (not implemented)
#           - lumin bio source including photon emission
#           - act lumin bio source including photon emission with sigmoid
#           - uni source including position model drawn from uni distr.
#           - gauss source including position model drawn from gauss distr.
#
# NOTE: multiple bio sources need to be inherited to build custom source.
# EXAMPLE: 
#
#class CustomSource(LS.ActLuminBioSource, LS.UniBioSource):
#
#    def __init__(self, domain, padding=1.5, source_id=0,
#                position0=None,
#                dct_velocity=None,
#                dct_ps=None,
#                t0_range=[[0,1]]):
#
#        start_time = time.time()
#        super().__init__(domain=domain, 
#                            padding=padding, 
#                            source_id=source_id, 
#                            position0=position0,
#                            dct_velocity=dct_velocity,
#                            dct_ps=dct_ps, 
#                            t0_range=t0_range)
#        
#
#        self.init_time = time.time() - start_time
#        print("Initialize:\tCustomSource\tComputation time: " +
#            str(self.init_time * 1000) + str(" ms"))
################################################################################

import nifty6 as ift
import numpy as np
from ANTARES.utils import nifty as nifty_utils
import time

################################################################################
# Source (Basic building block)
# only consisting of domain
################################################################################

class Source:

    def __init__(self, domain, padding_factor=1.5, source_id=0,  *args, **kwargs):
        """
        Creates a Source with inherent attributes
            - time space
            - padded time space
            - padding mask
        """

        n_pixels = domain[0].shape[0]
        dt = domain[0].distances[0]
        n_padding = int(n_pixels * padding_factor)

        # Attributes
        self.id = source_id
        self.padding_factor = padding_factor
        self.time_domain = domain
        self.time_space = domain[0]
        self.dt = dt
        self.n_pixels = n_pixels
        self.n_padding = n_padding

        # Spaces
        self.time_padding_space = ift.RGSpace(n_padding, dt)
        self.time_padding_domain = ift.DomainTuple.make(self.time_padding_space)

        # padding mask
        self.padding_mask = self._init_padding_mask(n_pixels, n_padding)

    def __repr__(self):
        s = 'Source%s' % str(self.id)
        return s

    def _init_padding_mask(self, n_pixels, n_padding):
        """
        Deletes padded pixels.
        Used to deal with periodicity.

        :param n_pixels: output size, n_pixels pixel field
        :param n_padding: input size, takes n_padding pixel field as input
        :return: n_pixels field
        """

        # initialize mask
        front_pad = int((n_padding - n_pixels)/2)
        back_pad = int(n_padding - front_pad - n_pixels)
        mask = np.hstack([np.ones(front_pad), np.zeros(n_pixels), np.ones(back_pad)])

        # Response
        # Masking operator
        # to model that parts of the field have not been observed
        mask = ift.Field.from_raw(self.time_padding_space, mask)
        mask = nifty_utils.TimeMaskOperator(mask, self.time_domain)

        return mask


################################################################################
# NoiseSource (Model)
# building the model of differen noise sources
#
# MISSING: _init_lumin (lumin model)
################################################################################

class NoiseSource(Source):

    def __init__(self, domain, padding=1.5, source_id=0, dct_lumin=None):
        """
        Creates a NoiseSource with inherent attributes
            - time space
            - padded time space
            - padding mask
            - luminosity (not implemented)
        """
        start_time = time.time()
        super().__init__(domain, padding, source_id)

        # Dictionaries
        self.dct_lumin = dct_lumin

        # Initialize inherent fields
        self.lumin = self._init_lumin()

        # Initialize signal field (MultiField of all inherent fields)
        self.signal, self.signal_time_domain = self._init_signal_field()

        self.init_time = time.time() - start_time
        print("Initialize:\tNoiseSource\tComputation time: " +
              str(self.init_time * 1000) + str(" ms"))

    def __repr__(self):
        s = 'NoiseSource%s' % str(self.id)
        return s

    def _init_signal_field(self):

        """
        Joint of cartesian coordinate spaces.

        :return: signal as MultiField, time domain
        """

        signal_time_domain = ift.MultiDomain.make(
            {'L': self.time_space})

        signal = ift.FieldAdapter(signal_time_domain, 'L')(self.lumin)

        return signal, signal_time_domain

    def _init_lumin(self):
        """
        Initializing field of luminosity.

        :return:
        """

        raise NotImplementedError


################################################################################
# ConstNoiseSource
#
# IMPLEMENTED: _init_lumin as constant source
################################################################################

class ConstNoiseSource(NoiseSource):

    def __init__(self, domain, padding=1.5, source_id=0,
                 dct_lumin=None):
        """
        Creates a NoiseSource with inherent attributes
            - time space
            - padded time space
            - padding mask
            - luminosity: correlated field
        """
        start_time = time.time()

        # Init
        super().__init__(domain, padding=padding, source_id=source_id,
                         dct_lumin=dct_lumin)

        self.init_time = time.time() - start_time
        print("Initialize:\tNoiseSource\tComputation time: " +
              str(self.init_time * 1000) + str(" ms"))

    def __repr__(self):
        s = 'ConstNoiseSource%s' % str(self.id)
        return s

    def _init_lumin(self):
        """
        Initializing correlated field of luminosity.
        Current implementation: correlated field

        :return:
        """

        # Make correlated field with given dictionary
        expander = ift.VdotOperator(ift.full(self.time_padding_domain, 1))
        domain = ift.DomainTuple.scalar_domain()

        # eff mean
        lumin_mean = ift.Adder(ift.full(domain, self.dct_lumin[0]))
        lumin_mean_xi = ift.FieldAdapter(domain, 'lumin_mean_xi_' + str(self.id))

        lumin_mean = lumin_mean @ lumin_mean_xi

        # eff mean
        if self.dct_lumin[1]==0 or self.dct_lumin[2]==0:
            lumin_std_mean = ift.Adder(ift.full(domain, self.dct_lumin[1]))
            lumin_std_std = ift.ScalingOperator(domain, self.dct_lumin[2])
            
            lumin_std_xi = ift.FieldAdapter(domain, 'lumin_std_xi_' + str(self.id))
            lumin_std = (lumin_std_mean @ lumin_std_std @ lumin_std_xi)
        else:
            lumin_std_mean = ift.Adder(ift.full(domain, np.log(self.dct_lumin[1]**2/np.sqrt(self.dct_lumin[1]**2 + self.dct_lumin[2]**2))))
            lumin_std_std = ift.ScalingOperator(domain, np.sqrt(np.log(1+self.dct_lumin[2]**2/self.dct_lumin[1]**2)))
            
            lumin_std_xi = ift.FieldAdapter(domain, 'lumin_std_xi_' + str(self.id))
            lumin_std = (lumin_std_mean @ lumin_std_std @ lumin_std_xi).exp()


        lumin_xi = ift.FieldAdapter(domain, 'lumin_xi_' + str(self.id))
        lumin = (lumin_mean + lumin_std * lumin_xi)
        lumin = expander.adjoint(lumin)

        #cf_lumin = ift.CorrelatedFieldMaker.make(**self.dct_lumin[0],
        #                                         prefix='lumin_' + str(self.id))
        #cf_lumin.add_fluctuations(target_subdomain=self.time_padding_space,
        #                          **self.dct_lumin[1])
        #lumin = cf_lumin.finalize()


        return self.padding_mask(lumin)


################################################################################
# ExpNoiseSource
#
# IMPLEMENTED: _init_lumin as log normal source
################################################################################

class ExpNoiseSource(NoiseSource):

    def __init__(self, domain, padding=1.5, source_id=0, dct_lumin=None):
        """
        Creates a NoiseSource with inherent attributes
            - time space
            - padded time space
            - padding mask
            - luminosity: exp(correlated field)
        """
        start_time = time.time()
        super().__init__(domain, padding, source_id, dct_lumin=dct_lumin)


        self.init_time = time.time() - start_time
        print("Initialize:\tNoiseSource\tComputation time: " +
              str(self.init_time * 1000) + str(" ms"))

    def __repr__(self):
        s = 'ExpNoiseSource%s' % str(self.id)
        return s

    def _init_lumin(self):
        """
        Initializing correlated field of luminosity.
        Current implementation: exp(correlated field)

        :return:
        """

        # Make correlated field with given dictrionary
        cf_lumin = ift.CorrelatedFieldMaker.make(**self.dct_lumin[0],
                                                 prefix='lumin_' + str(self.id))
        cf_lumin.add_fluctuations(target_subdomain=self.time_padding_space,
                                  **self.dct_lumin[1])
        lumin = cf_lumin.finalize()

        return self.padding_mask(lumin).exp()

################################################################################
# BioSource (Model)
#
# IMPLEMENTED: correlated movement model (on/off)
# MISSING: start postion model
################################################################################

class BioSource(Source):


    def __init__(self, domain, padding=1.5, source_id=0,
                 position0=None,
                 dct_velocity=None, *args, **kwargs):
        """
        Creates a BioObject with inherent attributes
            - time space
            - padded time space
            - padding mask
            - movement as pos (implemented)
            - starting position pos0 (not implemenmted)
        """
        start_time = time.time()
        super().__init__(domain, padding, source_id,  *args, **kwargs)

        # Init values
        self.position0 = position0
        self.dct_velocity = dct_velocity

        # Initialize position model
        self.x0, self.y0, self.z0, self.x, self.y, self.z = self._init_position()

        # Combining to MultiFields
        self.pos0, self.pos0_time_domain, self.pos, self.pos_time_domain = self._init_position_field()
        

        self.init_time = time.time() - start_time
        print("Initialize:\tBioObject\tComputation time: " +
              str(self.init_time * 1000) + str(" ms"))

    def __repr__(self):
        s = 'BioSource%s' % str(self.id)
        return s

    def _init_position_field(self):
        """
        Joint of cartesian coordinate spaces.

        :return: position as MultiField
        """

        pos_time_domain = ift.MultiDomain.make(
            {'x': self.time_space,
             'y': self.time_space,
             'z': self.time_space})

        pos = ift.FieldAdapter(pos_time_domain, 'x')(self.x) + \
              ift.FieldAdapter(pos_time_domain, 'y')(self.y) + \
              ift.FieldAdapter(pos_time_domain, 'z')(self.z)

        pos0_time_domain = ift.MultiDomain.make(
            {'x0': self.time_space,
             'y0': self.time_space,
             'z0': self.time_space})

        pos0 = ift.FieldAdapter(pos0_time_domain, 'x0')(self.x0) + \
              ift.FieldAdapter(pos0_time_domain, 'y0')(self.y0) + \
              ift.FieldAdapter(pos0_time_domain, 'z0')(self.z0)

        return pos0, pos0_time_domain, pos, pos_time_domain


    def _init_position(self):
        """
        Position field of BioObject

        :return: x,y,z
        """
        
        if self.dct_velocity is not None:
            x0, y0, z0 = self._init_position_model()
            corr_x, corr_y, corr_z = self._init_corr_v_x_y_z_field()
            x = x0 + corr_x
            y = y0 + corr_y
            z = z0 + corr_z
        else:
            x0, y0, z0 = self._init_position_model()
            x = x0
            y = y0
            z = z0

        return x0, y0, z0, x, y, z

    def _init_position_model(self):

        raise NotImplementedError

    def _init_corr_v_x_y_z_field(self):
        """
        Movement of BioObject

        :return: delta x, delta y, delta z
        """

        vec = []
        vec0 = []

        for i in range(self.n_pixels):
            m = self.dt * np.hstack([np.ones(i + 1), np.zeros(self.n_pixels - i - 1)])
            if i > 0:
                time_matrix = np.vstack([time_matrix, m])
            else:
                time_matrix = m

        for i, pos in enumerate(['x', 'y', 'z']):

            if self.dct_velocity is not None:

                cf_v = ift.CorrelatedFieldMaker.make(**self.dct_velocity[0],
                                                    prefix='v' + pos + '_' + str(self.id))
                cf_v.add_fluctuations(target_subdomain=self.time_padding_space,
                                    **self.dct_velocity[1])
                v = cf_v.finalize()

                x = ift.MatrixProductOperator(self.time_space, time_matrix) @ self.padding_mask(v)

            # Start position
            vec.append((x).clip(a_min=-200, a_max=200))

        return vec[0], vec[1], vec[2]

################################################################################
# LuminBioSource (Model)
#
# IMPLEMENTED: lumin model
# MISSING: start postion model
################################################################################

class LuminBioSource(BioSource):


    def __init__(self, domain, padding=1.5, source_id=0,
                 position0=None,
                 dct_velocity=None,
                 dct_ps=None,
                 *args, **kwargs):
        """
        Creates a BioObject with inherent attributes
            - time space
            - padded time space
            - padding mask
            - luminosity with activation
        """
        start_time = time.time()
        super().__init__(domain, padding, source_id, position0, dct_velocity, *args, **kwargs)

        # Init values
        self.dct_ps = dct_ps

        self.lumin = self._init_lumin()

        # Combining to MultiField
        self.signal, self._signal_time_domain = self._init_signal_field()

        self.init_time = time.time() - start_time
        print("Initialize:\tBioObject\tComputation time: " +
              str(self.init_time * 1000) + str(" ms"))

    def _init_signal_field(self):
        """
        Joint of cartesian coordinate spaces.

        :return: signal as MultiField
        """

        signal_time_domain = ift.MultiDomain.make(
            {'x': self.time_space,
             'y': self.time_space,
             'z': self.time_space,
             'L': self.time_space,
            })

        signal = ift.FieldAdapter(signal_time_domain, 'x')(self.x) + \
                 ift.FieldAdapter(signal_time_domain, 'y')(self.y) + \
                 ift.FieldAdapter(signal_time_domain, 'z')(self.z) + \
                 ift.FieldAdapter(signal_time_domain, 'L')(self.lumin)

        return signal, signal_time_domain
    
    def _init_lumin(self):

        lumin, self.lumin0, self.log_lumin= \
                self._init_lumin_burst(burst_no=0)

        return lumin


    def _init_lumin_burst(self, burst_no):
        """
        Initializing correlated field of luminosity.
        Current implementation: exp((HT(A * xi) + offset) * sigmoid)

        :return:
        """

        # Define harmonic space and harmonic transform
        harmonic_space = self.time_padding_space.get_default_codomain()
        HT = ift.HarmonicTransformOperator(harmonic_space, self.time_padding_space)

        p_space = ift.PowerSpace(harmonic_space)
        pd = ift.PowerDistributor(harmonic_space, p_space)
        
        # Lumin0
        lumin0 = ift.Adder(self.dct_ps['offset'], domain=self.time_domain)

        def sqrtps(k):

            r = 1/(k[1:])**self.dct_ps['plaw']
            sqrtps = self.dct_ps['amp']*r/np.sqrt(np.sum(r**2))
            
            return np.hstack([self.dct_ps['zeromode']/k[1], sqrtps/k[1]])

        #a = ift.makeField(p_space, ps(k, a, var, plaw=plaw))
        a = ift.PS_field(p_space, sqrtps)
        A = ift.makeOp(pd(a))
        lumin_xi = ift.FieldAdapter(harmonic_space, 'lumin_xi' + str(self.id) + '_' + str(burst_no))

        log_lumin = self.padding_mask(HT(A @ lumin_xi))

        # Final
        lumin = ift.exp(lumin0 @ log_lumin)

        return lumin, \
            lumin0, \
            log_lumin

################################################################################
# ActLuminBioSource (Model)
#
# IMPLEMENTED: lumin model with activation function
# MISSING: start postion model
################################################################################

class ActLuminBioSource(BioSource):


    def __init__(self, domain, padding=1.5, source_id=0,
                 position0=None,
                 dct_velocity=None,
                 dct_ps=None,
                 t0_range=[0,1], *args, **kwargs):
        """
        Creates a BioObject with inherent attributes
            - time space
            - padded time space
            - padding mask
            - luminosity with activation
        """
        start_time = time.time()
        super().__init__(domain, padding, source_id, position0, dct_velocity, *args, **kwargs)

        # Init values
        self.t0_range = t0_range
        self.dct_ps = dct_ps

        self.lumin, self.act_lumin = self._init_lumin()

        # Combining to MultiField
        self.signal, self._signal_time_domain = self._init_signal_field()

        self.init_time = time.time() - start_time
        print("Initialize:\tBioObject\tComputation time: " +
              str(self.init_time * 1000) + str(" ms"))

    def _init_signal_field(self):
        """
        Joint of cartesian coordinate spaces.

        :return: signal as MultiField
        """

        signal_time_domain = ift.MultiDomain.make(
            {'x': self.time_space,
             'y': self.time_space,
             'z': self.time_space,
             'L': self.time_space,
             'act_lumin': self.time_space})

        signal = ift.FieldAdapter(signal_time_domain, 'x')(self.x) + \
                 ift.FieldAdapter(signal_time_domain, 'y')(self.y) + \
                 ift.FieldAdapter(signal_time_domain, 'z')(self.z) + \
                 ift.FieldAdapter(signal_time_domain, 'L')(self.lumin) + \
                 ift.FieldAdapter(signal_time_domain, 'act_lumin')(self.act_lumin)

        return signal, signal_time_domain
    
    def _init_lumin(self):

        if len(self.t0_range) == 1:
            lumin, self.lumin0, self.log_lumin, act_lumin = \
                self._init_lumin_burst(t0_range=self.t0_range[0], burst_no=0)
        else:
            self.lumin0 = []
            self.log_lumin = []
            for i, t0 in enumerate(self.t0_range):
                if i == 0:
                    lumin, lumin0, log_lumin, act_lumin = \
                        self._init_lumin_burst(t0_range=t0, burst_no=i)
                    self.lumin0.append(lumin0)
                    self.log_lumin.append(log_lumin)
                else:
                    lumin_tmp, lumin0, log_lumin, act_lumin_tmp = \
                        self._init_lumin_burst(t0_range=t0, burst_no=i)
                    lumin = lumin  + lumin_tmp
                    act_lumin = act_lumin + act_lumin_tmp
                    self.lumin0.append(lumin0)
                    self.log_lumin.append(log_lumin)

        return lumin, act_lumin


    def _init_lumin_burst(self, t0_range, burst_no):
        """
        Initializing correlated field of luminosity.
        Current implementation: exp((HT(A * xi) + offset) * sigmoid)

        :return:
        """

        # Define harmonic space and harmonic transform
        harmonic_space = self.time_padding_space.get_default_codomain()
        HT = ift.HarmonicTransformOperator(harmonic_space, self.time_padding_space)

        p_space = ift.PowerSpace(harmonic_space)
        pd = ift.PowerDistributor(harmonic_space, p_space)

        # Helpers
        dummy_domain = ift.DomainTuple.scalar_domain()
        expander = ift.VdotOperator(ift.full(self.time_domain, 1.))
        
        # Time
        t = ift.makeField(self.time_domain, np.arange(self.n_pixels) * self.dt)
        t_scal = ift.makeOp(t)
        t_add = ift.Adder(t)

        # Activation
        t0 = ift.FieldAdapter(dummy_domain, 't0_' + str(self.id) + '_' + str(burst_no))
        uni = nifty_utils.Uniformizer(dummy_domain, t0_range[0] * self.n_pixels * self.dt, 
        t0_range[1] * self.n_pixels * self.dt)

        # inc_rate
        inc_xi = ift.FieldAdapter(dummy_domain, 'inc' + '_xi_' + str(self.id) + '_' + str(burst_no))
        uni_inc = nifty_utils.Uniformizer(dummy_domain, 4, 16)
        inc = expander.adjoint(uni_inc @ inc_xi)

        inc = ift.ScalingOperator(domain=self.time_domain, factor=10)

        act_lumin = (inc @ (t_add @ -expander.adjoint(uni(t0))))

        # Lumin0
        lumin0 = ift.Adder(self.dct_ps['offset'], domain=self.time_domain)

        def sqrtps(k):

            r = 1/(k[1:])**self.dct_ps['plaw']
            sqrtps = self.dct_ps['amp']*r/np.sqrt(np.sum(r**2))
            
            return np.hstack([self.dct_ps['zeromode']/k[1], sqrtps/k[1]])

        #a = ift.makeField(p_space, ps(k, a, var, plaw=plaw))
        a = ift.PS_field(p_space, sqrtps)
        A = ift.makeOp(pd(a))
        lumin_xi = ift.FieldAdapter(harmonic_space, 'lumin_xi' + str(self.id) + '_' + str(burst_no))

        log_lumin = self.padding_mask(HT(A @ lumin_xi))

        # Final
        lumin = ift.exp((lumin0 @ log_lumin) * (act_lumin).sigmoid())

        return lumin, \
            lumin0, \
            log_lumin, \
            act_lumin.sigmoid()


################################################################################
# UniBioSource (Model)
#
# IMPLEMENTED: start positin model from uni distr.
# MISSING: lumin model
################################################################################

class UniBioSource(BioSource):


    def __init__(self, domain, padding=1.5, source_id=0,
                 position0=None,
                 dct_velocity=None,
                 *args, **kwargs):
        """
        Creates a BioObject with inherent attributes
            - time space
            - padded time space
            - padding mask
            - position from uniform
        """
        start_time = time.time()
        super().__init__(domain, padding, source_id, position0, dct_velocity, *args, **kwargs)

        self.init_time = time.time() - start_time
        print("Initialize:\tBioObject\tComputation time: " +
              str(self.init_time * 1000) + str(" ms"))


    def _init_position_model(self):
        """
        Position field of BioObject

        :return: x,y,z
        """

        vec = []

        for i, pos in enumerate(['x', 'y', 'z']):

            # x0 mean
            dummy_domain = ift.DomainTuple.scalar_domain()
            expander = ift.VdotOperator(ift.full(self.time_domain, 1.))

            # Activation
            x0_xi = ift.FieldAdapter(dummy_domain, pos + '0_xi_' + str(self.id))
            uni_x0 = nifty_utils.Uniformizer(dummy_domain, self.position0[i][0], 
                                                            self.position0[i][1])

            x0 = expander.adjoint(uni_x0(x0_xi))

            # Start position
            vec.append((x0).clip(a_min=-200, a_max=200))

        return vec[0], vec[1], vec[2]

    
################################################################################
# GaussBioSource (Model)
#
# IMPLEMENTED: start positin model from gauss distr.
# MISSING: lumin model
################################################################################

class GaussBioSource(BioSource):


    def __init__(self, domain, padding=1.5, source_id=0,
                 position0=None,
                 dct_velocity=None,
                 *args, **kwargs):
        """
        Creates a BioObject with inherent attributes
            - time space
            - padded time space
            - padding mask
            - position from gaussian
        """
        start_time = time.time()

        super().__init__(domain, padding, source_id, position0, dct_velocity, *args, **kwargs)

        self.init_time = time.time() - start_time
        print("Initialize:\tBioObject\tComputation time: " +
              str(self.init_time * 1000) + str(" ms"))

    def __repr__(self):
        s = 'BioSource%s' % str(self.id)
        return s


    def _init_position_model(self):
        """
        Position field of BioObject

        :return: x,y,z
        """

        vec = []
        init_gauss_position = self.position0[0]
        init_gauss_param = self.position0[1]

        for i, pos in enumerate(['x', 'y', 'z']):

            # x0 mean
            dummy_domain = ift.DomainTuple.scalar_domain()
            expander = ift.VdotOperator(ift.full(self.time_domain, 1.))

            x0_mean_mean = ift.Adder(ift.full(dummy_domain, init_gauss_position[i]))
            x0_mean_std = ift.ScalingOperator(dummy_domain, init_gauss_param[0])
            x0_mean_xi = ift.FieldAdapter(dummy_domain, pos + '0' + '_mean_xi_' + str(self.id))

            x0_mean = x0_mean_mean @ x0_mean_std @ x0_mean_xi

            # eff mean
            x0_std_mean = ift.Adder(ift.full(dummy_domain, init_gauss_param[1]))
            x0_std_std = ift.ScalingOperator(dummy_domain, init_gauss_param[2])
            x0_std_xi = ift.FieldAdapter(dummy_domain, pos + '0' + '_std_xi_' + str(self.id))

            #x0 = x0_mean_mean @ x0_mean_std @ x0_mean_xi
            #x0 = expander.adjoint @ x0

            x0_std = (x0_std_mean @ x0_std_std @ x0_std_xi)

            # eff
            x0_xi = ift.FieldAdapter(dummy_domain, pos + '0' + '_xi_' + str(self.id))

            x0 = expander.adjoint @ (x0_mean + x0_std * x0_xi)

            # Start position
            vec.append((x0).clip(a_min=-200, a_max=200))

        return vec[0], vec[1], vec[2]