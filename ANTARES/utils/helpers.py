import os
import pickle
import sys
import importlib

###################################################################################################
# Helpers
###################################################################################################


def plot_save(plt, title, output_dir):
    """
    Save plot with input and output dir as args.

    :param plt:
    :param title:
    :param output_dir:
    :return:
    """

    if output_dir is None:
        plt.show()
    else:
        create_dir(output_dir)
        #print('Saving ' + title)
        plt.savefig(output_dir + '/' + title)
        plt.close()


def import_config(config):
    """
    Importing config file.

    :param config: file path
    :return: cfg module
    """

    package_path = config[:config.rfind('/')]
    module_name = config[config.rfind('/')+1:-3]
    sys.path.append(package_path)
    cfg = importlib.import_module(module_name, package=package_path)

    return cfg


def get_base():
    """
    Get machine name to set base path.

    :return: base directory
    """

    base_dir = -1

    if os.uname()[1] == '':
        base_dir = ''
    else:
        print('Could not recognize computer!\nPath to data not set')

    return base_dir


def load_globals(INPUT_DIR, l):

    if os.path.exists(INPUT_DIR + 'globals.pickle'):
        dict_globals = load(INPUT_DIR + 'globals.pickle')
        l.update(dict_globals)
    else:
        print("No such file: " + INPUT_DIR + 'globals.pickle')
        raise


def save_globals(OUTPUT_DIR, g, keys=None):

    if keys is None:
        keys = ['SEED', 'DATA_FILE', 'EFF_FILE', 'DATA_SAMPLE', 'LINES', 'OMS', 'OM_FILE',
                'OUTPUT_DIR', 'SOURCE_FILE', 'RESPONSES_FILE', 'STATS_FILE', 'DATA_OUTPUT_FILE',
                'KL_FILE',
                'N_SAMPLES', 'N_GLOBAL_ITERATIONS', 'MINIMIZATION_ITERATION_LIMIT',
                'N_LOCAL_ITERATIONS', 'MINIMIZATION_DELTA_E', 'SAMPLING_ITERATION_LIMIT',
                'SAMPLING_DELTA_E', 'RECON_N_SAMPLES', 'OM_ID', 'N_PIXELS']

    dict_globals = {}
    for key in keys:
        dict_globals[key] = g[key]
    save(dict_globals, 'globals.pickle', OUTPUT_DIR)


def create_dir(output_dir):
    """
    Creates directory after checking if it exists.

    :param output_dir: path to output dir
    :return:
    """

    if output_dir is not None:
        if os.path.isdir(output_dir):
            #print("Output directory " + output_dir + " exists.")
            flag = 0
        else:
            os.mkdir(output_dir)
            #print("Output directory " + output_dir + " created.")
            flag = 1

    return flag


def save(object, filename, output_dir):

    # Saving
    if output_dir is not None:
        #print('Saving ' + filename)
        create_dir(output_dir)
        pickle.dump(object, open(output_dir + '/' + filename, 'wb'))


def load(filename):

    # Loading
    # print('Loading ' + filename)
    object = pickle.load(open(filename, 'rb'))

    return object
