import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from . import helpers
import os
from mpl_toolkits.mplot3d import Axes3D
from ANTARES.models import LightSources as LS
from ANTARES.models import Response as R
plt.ioff()

# define detector color map
om_cmap = plt.get_cmap('nipy_spectral')

def set_size(subplots, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = 361 * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * subplots

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim

def plot_data(responses, xis, data, dataset, log_flag=False, title_prefix="", output_dir=None):
    """
    Plotting OMS data (Ground Truth, Mock data, reconstruction).

    :param oms:
    :param H:
    :param mock_xi:
    :param mock_data:
    :param title_prefix:
    :param output_dir:
    :return:
    """

    # fix colors of detectors
    colors = iter(om_cmap(np.linspace(0, 1, len(responses))))

    # fig size
    fig = plt.figure(figsize=(16,12))
    plt.title("Data reconstruction of dataset " + str(dataset) + str(title_prefix))

    for om_id in responses:
        c = next(colors)
        # Ground Truth
        if xis is not None:
            for xi in xis:
                if log_flag:
                    plt.plot(np.log10(responses[om_id][dataset].response.force(xi[0]).val),
                            label='om ' + str(om_id) + xi[1],
                            color=c, linestyle='dashed')
                else:
                    plt.plot(responses[om_id][dataset].response.force(xi[0]).val,
                            label='om ' + str(om_id) + xi[1],
                            color=c, linestyle='dashed')
        # Mock data
        if data is not None:
            if log_flag:
                plt.plot(np.log10(responses[om_id][dataset].mask.adjoint(data[om_id][dataset]).val),
                        label='om '+str(om_id)+'/data', color=c)
                plt.ylabel('logarithmic Photon hits')
                #plt.ylim([8,np.log(50000)])
            else:
                plt.plot(responses[om_id][dataset].mask.adjoint(data[om_id][dataset]).val,
                        label='om '+str(om_id)+'/data', color=c)
                plt.ylabel('Photon hits')
                #plt.ylim([0,50000])


    plt.xlabel('Time steps')
    plt.legend()

    # Saving
    helpers.plot_save(plt, "data_" + str(title_prefix) + ".png", output_dir)


def plot_signal(sources, oms, responses, xis, title_prefix="", output_dir=None):
    """
    Plotting signal data (position, Euler angels, multipole moments).

    :param sources:
    :param H:
    :param mock_xi:
    :param title_prefix:
    :param output_dir:
    :return:
    """

    for source in sources:
        signal = source.signal
        for key in signal.target.keys():

                fig, ax = plt.subplots(1, 1, sharex=True, figsize=(16,12))

                ax.set_title(key + ', Source: ' + str(source.id))

                if xis is not None:
                    for xi in xis:
                        ax.plot(signal.force(xi[0]).val[key],
                                xi[2], label=xi[1])

                ax.legend()
                ax.set_ylabel('Scalar')

                ax.set_xlabel('Time Steps')

                helpers.plot_save(plt, key + str(source.id)  + '_' + str(title_prefix),
                                  output_dir)

    plt.close()
    if oms is not None:
        signal_keys = []
        for key in oms[list(oms.keys())[0]].signal.target.keys():
            signal_keys.append(key)



        for key in signal_keys:
            fig, ax = plt.subplots(1, 1, sharex=True, figsize=(16,12))
            ax.set_title(key + xi[1])
            
            for om_id in oms:
                signal = oms[om_id].signal

                if xis is not None:
                    for xi in xis:
                        #ax.plot(signal.force(xi[0]).val[key],
                        #        xi[2], label=xi[1])
                        value = signal.force(xi[0]).val[key]
                        ax.plot(value, om_id[1], xi[2], label='recon {0}: {1:.4f}'.format(om_id, value))

                        if 'aniso' in key and 'std' in key:
                            ax.plot(oms[om_id].aniso_eff_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].aniso_eff_init[2]))
                        elif 'eff' in key and 'std' in key:
                            ax.plot(oms[om_id].eff_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].eff_init[2]))
                        elif 'l_att' in key and 'std' in key:
                            ax.plot(oms[om_id].l_att_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].l_att_init[2]))
                        elif 'aniso' in key:
                            ax.plot(oms[om_id].aniso_eff_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].aniso_eff_init[0]))
                        elif 'eff' in key:
                            ax.plot(oms[om_id].eff_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].eff_init[0]))
                        elif 'l_att_factor' in key:
                            ax.plot(np.exp(-1/oms[om_id].l_att_init[0]), om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, np.exp(-1/oms[om_id].l_att_init[0])))
                        elif 'l_att' in key:
                            ax.plot(oms[om_id].l_att_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].l_att_init[0]))

            ax.legend()
            ax.set_ylabel('OM Id')
            ax.set_xlabel('Scalar')

            om_id = np.array(list(oms.keys()))
            ax.set_yticks(om_id[:,1])
            ax.set_yticklabels(list(oms.keys()))

            helpers.plot_save(plt, key + "_" + str(title_prefix),
                                  output_dir)
    plt.close()
    if responses is not None:
        for om_id in responses:
            for dataset in range(len(responses[om_id])):
                signal = responses[om_id][dataset].signal
                for key in signal.target.keys():

                    fig = plt.figure(figsize=(16, 12))
                    plt.title(key + " - Signal of " + str(om_id) + str(title_prefix))

                    if xis is not None:
                        for xi in xis:
                            plt.plot(signal.force(xi[0]).val[key],
                                    xi[2], label=xi[1])

                    helpers.plot_save(plt, key + str(om_id) + "_" + str(dataset) +
                                      str(title_prefix),
                                      output_dir)


def plot_signal_stats8(samples, xis, sources, oms, title_prefix="", output_dir=None):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """

    for source in sources:
        mean, var = samples.sample_stat(source.signal)
        for key in mean.domain.keys():

            fig = plt.figure(figsize=(16, 12))
            plt.title(key + "Signal of " + source.__repr__() + str(title_prefix))

            if 'L' in key:
                if xis is not None:
                    for xi in xis:
                        plt.plot(np.log10(source.signal.force(xi[0]).val[key]), xi[2],
                                label=xi[1])
                plt.plot(np.log10(mean.val[key]), 'ro', label='recon')

                # error
                plt.plot(np.log10(mean.val[key] - np.sqrt(var.val[key])),
                        'r', linestyle='dashed', label='recon_std')
                plt.plot(np.log10(mean.val[key] + np.sqrt(var.val[key])),
                        'r', linestyle='dashed', label='recon_std')

                
                plt.xlabel('Time steps')
                plt.ylabel(key)

                plt.legend()

                helpers.plot_save(plt, source.__repr__() + '_' + key + str(title_prefix) + '_log',
                                output_dir)

                if xis is not None:
                    for xi in xis:
                        plt.plot(source.signal.force(xi[0]).val[key], xi[2],
                                label=xi[1])
                plt.plot(mean.val[key], 'ro', label='recon')

                # error
                plt.plot(mean.val[key] - np.sqrt(var.val[key]),
                        'r', linestyle='dashed', label='recon_std')
                plt.plot(mean.val[key] + np.sqrt(var.val[key]),
                        'r', linestyle='dashed', label='recon_std')

                
                plt.xlabel('Time steps')
                plt.ylabel(key)

                plt.legend()

                helpers.plot_save(plt, source.__repr__() + '_' + key + str(title_prefix),
                                output_dir)

            else:
                if xis is not None:
                    for xi in xis:
                        plt.plot(source.signal.force(xi[0]).val[key], xi[2],
                                label=xi[1])
                plt.plot(mean.val[key], 'ro', label='recon')

                # error
                plt.plot(mean.val[key] - np.sqrt(var.val[key]),
                        'r', linestyle='dashed', label='recon_std')
                plt.plot(mean.val[key] + np.sqrt(var.val[key]),
                        'r', linestyle='dashed', label='recon_std')

                plt.xlabel('Time steps')
                plt.ylabel(key)

                plt.legend()

                helpers.plot_save(plt, source.__repr__()  + '_' + key + str(title_prefix),
                                output_dir)

    mean, var = samples.sample_stat(list(oms.values())[0].signal)

    for key in mean.domain.keys():

        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(16, 12))
        ax.set_title(key + str(title_prefix))

        for om_id in oms:


            mean, var = samples.sample_stat(oms[om_id].signal)
            
            if xis is not None:
                for xi in xis:
                    ax.plot(oms[om_id].signal.force(xi[0]).val[key], xi[2], om_id[1],
                            label=om_id)
            ax.plot(mean.val[key], om_id[1], 'ko', label='recon {0}:{1:.4f}'.format(om_id, mean.val[key]))

            # error
            ax.plot(mean.val[key] - np.sqrt(var.val[key]), om_id[1],
                    'rx')
            ax.plot(mean.val[key] + np.sqrt(var.val[key]), om_id[1],
                    'rx')

            if 'aniso' in key and 'std' in key:
                ax.plot(oms[om_id].aniso_eff_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].aniso_eff_init[2]))
            elif 'eff' in key and 'std' in key:
                ax.plot(oms[om_id].eff_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].eff_init[2]))
            elif 'l_att' in key and 'std' in key:
                ax.plot(oms[om_id].l_att_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].l_att_init[2]))
            elif 'aniso' in key:
                ax.plot(oms[om_id].aniso_eff_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].aniso_eff_init[0]))
            elif 'eff' in key:
                ax.plot(oms[om_id].eff_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].eff_init[0]))
            elif 'l_att_factor' in key:
                ax.plot(np.exp(-1/oms[om_id].l_att_init[0]), om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, np.exp(-1/oms[om_id].l_att_init[0])))
            elif 'l_att' in key:
                ax.plot(oms[om_id].l_att_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].l_att_init[0]))



        ax.set_xlabel(key)
        ax.set_ylabel('OM')
        om_id = np.array(list(oms.keys()))
        ax.set_yticks(om_id[:,1])
        ax.set_yticklabels(list(oms.keys()))

        plt.legend()

        helpers.plot_save(plt, key + str(title_prefix),
                                output_dir)

def plot_signal_stats(sc, xis, oms, title_prefix="", output_dir=None):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """

    om_source_list = []
    for source in sc:
        if source=='H':
            continue

        elif 'OM' in source:
            om_source_list.append(source)
            continue

        elif 'KL' in source:
            continue

        for key in sc[source].mean.domain.keys():

            fig = plt.figure(figsize=(16, 12))
            plt.title(key + "Signal of " + source + str(title_prefix))

            if 'L' in key:
                if xis is not None:
                    for xi in xis:
                        plt.plot(np.log10(source.signal.force(xi[0]).val[key]), xi[2],
                                label=xi[1])
                plt.plot(np.log10(sc[source].mean.val[key]), 'ro', label='recon')

                # error
                plt.plot(np.log10(sc[source].mean.val[key] - np.sqrt(sc[source].var.val[key])),
                        'r', linestyle='dashed', label='recon_std')
                plt.plot(np.log10(sc[source].mean.val[key] + np.sqrt(sc[source].var.val[key])),
                        'r', linestyle='dashed', label='recon_std')

                
                plt.xlabel('Time steps')
                plt.ylabel(key)

                plt.legend()

                helpers.plot_save(plt, source + '_' + key + str(title_prefix) + '_log',
                                output_dir)

                if xis is not None:
                    for xi in xis:
                        plt.plot(source.signal.force(xi[0]).val[key], xi[2],
                                label=xi[1])
                plt.plot(sc[source].mean.val[key], 'ro', label='recon')

                # error
                plt.plot(sc[source].mean.val[key] - np.sqrt(sc[source].var.val[key]),
                        'r', linestyle='dashed', label='recon_std')
                plt.plot(sc[source].mean.val[key] + np.sqrt(sc[source].var.val[key]),
                        'r', linestyle='dashed', label='recon_std')

                
                plt.xlabel('Time steps')
                plt.ylabel(key)

                plt.legend()

                helpers.plot_save(plt, source + '_' + key + str(title_prefix),
                                output_dir)

            else:
                if xis is not None:
                    for xi in xis:
                        plt.plot(source.signal.force(xi[0]).val[key], xi[2],
                                label=xi[1])
                plt.plot(sc[source].mean.val[key], 'ro', label='recon')

                # error
                plt.plot(sc[source].mean.val[key] - np.sqrt(sc[source].var.val[key]),
                        'r', linestyle='dashed', label='recon_std')
                plt.plot(sc[source].mean.val[key] + np.sqrt(sc[source].var.val[key]),
                        'r', linestyle='dashed', label='recon_std')

                plt.xlabel('Time steps')
                plt.ylabel(key)

                plt.legend()

                helpers.plot_save(plt, source + '_' + key + str(title_prefix),
                                output_dir)

    for key in sc[om_source_list[0]].mean.domain.keys():

        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(16, 12))
        ax.set_title(key + str(title_prefix))

        for source in om_source_list:
            
            string_om_id = source[2:].split(',') 
            om_id = (int(string_om_id[0][1:]), int(string_om_id[1][:-1]))

            if xis is not None:
                for xi in xis:
                    ax.plot(source.signal.force(xi[0]).val[key], xi[2], om_id[1],
                            label=source)
            ax.plot(sc[source].mean.val[key], om_id[1], 'ko', label='recon {0}:{1:.4f}'.format(om_id, sc[source].mean.val[key]))

            # error
            ax.plot(sc[source].mean.val[key] - np.sqrt(sc[source].var.val[key]), om_id[1],
                    'rx')
            ax.plot(sc[source].mean.val[key] + np.sqrt(sc[source].var.val[key]), om_id[1],
                    'rx')

            if 'aniso' in key and 'std' in key:
                ax.plot(oms[om_id].aniso_eff_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].aniso_eff_init[2]))
            elif 'eff' in key and 'std' in key:
                ax.plot(oms[om_id].eff_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].eff_init[2]))
            elif 'l_att' in key and 'std' in key:
                ax.plot(oms[om_id].l_att_init[2], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].l_att_init[2]))
            elif 'aniso' in key:
                ax.plot(oms[om_id].aniso_eff_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].aniso_eff_init[0]))
            elif 'eff' in key:
                ax.plot(oms[om_id].eff_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].eff_init[0]))
            elif 'l_att_factor' in key:
                ax.plot(np.exp(-1/oms[om_id].l_att_init[0]), om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, np.exp(-1/oms[om_id].l_att_init[0])))
            elif 'l_att' in key:
                ax.plot(oms[om_id].l_att_init[0], om_id[1], 'bo', label='init {0}: {1:.4f}'.format(om_id, oms[om_id].l_att_init[0]))



        ax.set_xlabel(key)
        ax.set_ylabel('OM')
        om_id = np.array(list(oms.keys()))
        ax.set_yticks(om_id[:,1])
        ax.set_yticklabels(list(oms.keys()))

        plt.legend()

        helpers.plot_save(plt, key + str(title_prefix),
                                output_dir)


def plot_response_stats8(data, samples, responses, sources, log_flag=False, title_prefix="", output_dir=None):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """

    # fix colors of detectors
    colors = iter(om_cmap(np.linspace(0, 1, len(responses))))

    for source in sources:
        if isinstance(source, LS.BioSource):
            mean, var = samples.sample_stat(source.signal)
            t = np.argmax(mean.val['L'])
    
    flash_error = {}
    flash_mean_error = {}
    bkg_error = {}
    bkg_mean_error = {}
    full_error = {}
    full_mean_error = {}

    if responses is not None:

        for om_id in responses:

            c = next(colors)

            for dataset in range(len(responses[om_id])):

                key = 'response'
                response = responses[om_id][dataset]

                fig, (ax_response, ax_error) = plt.subplots(2, 1, sharex=True, figsize=(19,16))

                # Error plot
                ax_error.set_title("Error of response OM" + str(om_id) + str(title_prefix))

                d = response.mask.adjoint(data[om_id][dataset]).val
                mean, var = samples.sample_stat(response.signal)
                r = mean.val[key]

                error = (d - r)
                sigma = np.sqrt(r)

                rel_error = error/sigma
                flash_error[om_id] = np.sum(rel_error[t:])
                flash_mean_error[om_id] = np.mean(rel_error[t:])
                bkg_error[om_id] = np.sum(rel_error[:t])
                bkg_mean_error[om_id] = np.mean(rel_error[:t])
                full_error[om_id] = np.sum(rel_error)
                full_mean_error[om_id] = np.mean(rel_error)



                ax_error.plot(error/sigma,
                            label='mean: ' + str(np.mean(error/sigma)) + \
                                 ' median: ' + str(np.median(error/sigma)), color=c)

                ax_error.hlines(1, xmin=0, xmax=len(error), colors='r',
                            linestyles='solid', label=r'$\sigma =1$')
                ax_error.hlines(2, xmin=0, xmax=len(error), colors='r',
                            linestyles='dashed', label=r'$\sigma =2$')
                ax_error.set_ylim(-5, 5)
                ax_error.set_ylabel('error in terms of sigma')

                # Response plot
                ax_response.set_title("Reconstruction of response (H = " + " )" + str(title_prefix))

                if log_flag:
                    ax_response.plot(np.log10(d),
                            label='om ' + str(om_id) + '/data', color=c)
                    # mean
                    ax_response.plot(np.log10(r), 'ro', label='om ' + str(om_id) + '/recon')

                    # error
                    ax_response.plot(np.log10(r - np.sqrt(var.val[key])),
                            'r+', label='om ' + str(om_id) + '/recon_std')
                    ax_response.plot(np.log10(r + np.sqrt(var.val[key])),
                            'r+')

                else:
                    ax_response.plot(d,
                            label='om ' + str(om_id) + '/data', color=c)

                    # mean
                    ax_response.plot(r, 'ro', label='om ' + str(om_id) + '/recon')

                    # error
                    ax_response.plot(r - np.sqrt(var.val[key]),
                            'r+', label='om ' + str(om_id) + '/recon_std')
                    ax_response.plot(r + np.sqrt(var.val[key]),
                            'r+')

                ax_response.set_xlabel('Time steps')
                ax_response.set_ylabel('Photon hits')

                plt.legend()

                helpers.plot_save(plt, response.__repr__() + '_' + key + '_' + str(title_prefix),
                                  output_dir)


        fig, ax = plt.subplots(1, 1, sharex=True, figsize=(16,12))
        ax.set_title("Flash residual summary")

        for om_id in responses:
            ax.plot(flash_mean_error[om_id], om_id[1], 'ko', 
                            label='{0} mean: {1:.2f}, sum: {2:.2f}'.format(om_id, flash_mean_error[om_id], flash_error[om_id]))

            ax.legend()
        
        ax.set_xlabel('error in sigma')
        ax.set_ylabel('OM')
        om_id = np.array(list(responses.keys()))
        ax.set_yticks(om_id[:,1])
        ax.set_yticklabels(list(responses.keys()))

        helpers.plot_save(plt, 'flash_residual_summary_' + str(title_prefix),
                                  output_dir)

        fig, ax = plt.subplots(1, 1, sharex=True, figsize=(16,12))
        ax.set_title("Background residual summary")

        for om_id in responses:
            ax.plot(bkg_mean_error[om_id], om_id[1], 'ko', 
                            label='{0} mean: {1:.2f}, sum: {2:.2f}'.format(om_id, bkg_mean_error[om_id], bkg_error[om_id]))

            ax.legend()
        
        ax.set_xlabel('error in sigma')
        ax.set_ylabel('OM')
        om_id = np.array(list(responses.keys()))
        ax.set_yticks(om_id[:,1])
        ax.set_yticklabels(list(responses.keys()))

        helpers.plot_save(plt, 'background_residual_summary_' + str(title_prefix),
                                  output_dir)

        fig, ax = plt.subplots(1, 1, sharex=True, figsize=(16,12))
        ax.set_title("Full residual summary")

        for om_id in responses:
            ax.plot(full_mean_error[om_id], om_id[1], 'ko', 
                            label='{0} mean: {1:.2f}, sum: {2:.2f}'.format(om_id, full_mean_error[om_id], full_error[om_id]))

            ax.legend()
        
        ax.set_xlabel('error in sigma')
        ax.set_ylabel('OM')
        om_id = np.array(list(responses.keys()))
        ax.set_yticks(om_id[:,1])
        ax.set_yticklabels(list(responses.keys()))

        helpers.plot_save(plt, 'full_residual_summary_' + str(title_prefix),
                                  output_dir)

def plot_response_stats(data, sc, responses, sources, log_flag=False, title_prefix="", output_dir=None):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """

    # fix colors of detectors
    colors = iter(om_cmap(np.linspace(0, 1, len(responses))))

    for source in sources:
        if isinstance(source, LS.BioSource):
            source = source.__repr__()
            L = sc[source].mean.val['L']
            t = np.argmax(sc[source].mean.val['L'])
    
    flash_error = {}
    flash_mean_error = {}
    bkg_error = {}
    bkg_mean_error = {}
    full_error = {}
    full_mean_error = {}

    if responses is not None:

        for om_id in responses:

            c = next(colors)

            for dataset in range(len(responses[om_id])):

                key = 'response'
                source = responses[om_id][dataset].__repr__()

                fig, (ax_response, ax_error) = plt.subplots(2, 1, sharex=True, figsize=(19,16))

                # Error plot
                ax_error.set_title("Error of response OM" + str(om_id) + str(title_prefix))

                d = responses[om_id][dataset].mask.adjoint(data[om_id][dataset]).val
                idx = d > 1
                t = np.argmax(L[idx])
                r = sc[source].mean.val[key]

                error = (d - r)
                sigma = np.sqrt(r)

                rel_error = error/sigma
                flash_error[om_id] = np.sum(rel_error[t:])
                flash_mean_error[om_id] = np.mean(rel_error[t:])
                bkg_error[om_id] = np.sum(rel_error[:t])
                bkg_mean_error[om_id] = np.mean(rel_error[:t])
                full_error[om_id] = np.sum(rel_error)
                full_mean_error[om_id] = np.mean(rel_error)



                ax_error.plot(error/sigma,
                            label='mean: ' + str(np.mean(error[idx]/sigma[idx])) + \
                                 ' median: ' + str(np.median(error[idx]/sigma[idx])), color=c)

                ax_error.hlines(1, xmin=0, xmax=len(error), colors='r',
                            linestyles='solid', label=r'$\sigma =1$')
                ax_error.hlines(2, xmin=0, xmax=len(error), colors='r',
                            linestyles='dashed', label=r'$\sigma =2$')
                ax_error.set_ylim(-5, 5)
                ax_error.set_ylabel('error in terms of sigma')

                # Response plot
                ax_response.set_title("Reconstruction of response (H = " + str(sc['H'].mean.val) + " )" + str(title_prefix))

                if log_flag:
                    ax_response.plot(np.log10(d),
                            label='om ' + str(om_id) + '/data', color=c)
                    # mean
                    ax_response.plot(np.log10(r), 'ro', label='om ' + str(om_id) + '/recon')

                    # error
                    ax_response.plot(np.log10(r - np.sqrt(sc[source].var.val[key])),
                            'r+', label='om ' + str(om_id) + '/recon_std')
                    ax_response.plot(np.log10(r + np.sqrt(sc[source].var.val[key])),
                            'r+')

                else:
                    ax_response.plot(d,
                            label='om ' + str(om_id) + '/data', color=c)

                    # mean
                    ax_response.plot(r, 'ro', label='om ' + str(om_id) + '/recon')

                    # error
                    ax_response.plot(r - np.sqrt(sc[source].var.val[key]),
                            'r+', label='om ' + str(om_id) + '/recon_std')
                    ax_response.plot(r + np.sqrt(sc[source].var.val[key]),
                            'r+')

                ax_response.set_xlabel('Time steps')
                ax_response.set_ylabel('Photon hits')

                plt.legend()

                helpers.plot_save(plt, source + '_' + key + '_' + str(title_prefix),
                                  output_dir)


        fig, ax = plt.subplots(1, 1, sharex=True, figsize=(16,12))
        ax.set_title("Flash residual summary")

        for om_id in responses:
            ax.plot(flash_mean_error[om_id], om_id[1], 'ko', 
                            label='{0} mean: {1:.2f}, sum: {2:.2f}'.format(om_id, flash_mean_error[om_id], flash_error[om_id]))

            ax.legend()
        
        ax.set_xlabel('error in sigma')
        ax.set_ylabel('OM')
        om_id = np.array(list(responses.keys()))
        ax.set_yticks(om_id[:,1])
        ax.set_yticklabels(list(responses.keys()))

        helpers.plot_save(plt, 'flash_residual_summary_' + str(title_prefix),
                                  output_dir)

        fig, ax = plt.subplots(1, 1, sharex=True, figsize=(16,12))
        ax.set_title("Background residual summary")

        for om_id in responses:
            ax.plot(bkg_mean_error[om_id], om_id[1], 'ko', 
                            label='{0} mean: {1:.2f}, sum: {2:.2f}'.format(om_id, bkg_mean_error[om_id], bkg_error[om_id]))

            ax.legend()
        
        ax.set_xlabel('error in sigma')
        ax.set_ylabel('OM')
        om_id = np.array(list(responses.keys()))
        ax.set_yticks(om_id[:,1])
        ax.set_yticklabels(list(responses.keys()))

        helpers.plot_save(plt, 'background_residual_summary_' + str(title_prefix),
                                  output_dir)

        fig, ax = plt.subplots(1, 1, sharex=True, figsize=(16,12))
        ax.set_title("Full residual summary")

        for om_id in responses:
            ax.plot(full_mean_error[om_id], om_id[1], 'ko', 
                            label='{0} mean: {1:.2f}, sum: {2:.2f}'.format(om_id, full_mean_error[om_id], full_error[om_id]))

            ax.legend()
        
        ax.set_xlabel('error in sigma')
        ax.set_ylabel('OM')
        om_id = np.array(list(responses.keys()))
        ax.set_yticks(om_id[:,1])
        ax.set_yticklabels(list(responses.keys()))

        helpers.plot_save(plt, 'full_residual_summary_' + str(title_prefix),
                                  output_dir)


def plot_lumin_response(sc, oms, sources, title_prefix="", output_dir=None):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """

    for source in sources:

        if isinstance(source, LS.BioSource):
            n_pixels = source.time_domain.shape[0]
            source = source.__repr__()
            for axis in ['y', 'z']:

                colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
                plt.figure(figsize=(16, 16))
                plt.title('Movement - x' + axis + ' axis of source: ' + source)
                for _ in range(n_pixels):
                    plt.scatter(sc[source].mean.val['x'][_], sc[source].mean.val[axis][_],
                            marker='o', s=1000*sc[source].mean.val['L'][_]/
                                          np.max(sc[source].mean.val['L']),
                            alpha=n_pixels / (n_pixels + 2 * _))

                for om_id in oms:
                    i_axis = 1 if axis == 'y' else 2
                    plt.plot([oms[om_id].pos[0],
                              oms[om_id].pos[0] + oms[om_id].dir[0]],
                             [oms[om_id].pos[i_axis],
                              oms[om_id].pos[i_axis] + oms[om_id].dir[i_axis]],
                             label='om ' + str(om_id), color=next(colors))
                plt.legend()
                plt.xlabel('x')
                plt.ylabel(axis)
                plt.axis('equal')

                helpers.plot_save(plt, 'x' + axis + source + title_prefix,
                                  output_dir)


def plot_movement_3d(sc, sources, oms, KL, title, error_bars=False):
    """
    Plotting 3D movement depending on arguments with or without reconstruction.

    :param signal:
    :param oms:
    :param KL:
    :param xi:
    :param n_pixels:
    :param title:
    :return:
    """
    om_cmap = plt.get_cmap('nipy_spectral')

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ax = fig.add_subplot(111, projection='3d')
    ax.set_title(title)

    for source in sources:
        if isinstance(source, LS.BioSource):
            n_pixels = source.time_domain.shape[0]
            source = source.__repr__()

            lumin_cmap = plt.get_cmap('Greens')
            lumin_colors = iter(lumin_cmap(np.linspace(0, 1, n_pixels)))
            for _ in range(n_pixels):
                if KL is not None:
                    ax.scatter(sc[source].mean.val['x'][_],
                               sc[source].mean.val['y'][_],
                               sc[source].mean.val['z'][_],
                               marker='o',
                               s=1000 * sc[source].mean.val['L'][_] /
                                 np.max(sc[source].mean.val['L']),
                               alpha=0.5, color=next(lumin_colors))

                    # error

                    if error_bars:

                        ax.plot([sc[source].mean.val['x'][_] - np.sqrt(sc[source].var.val['x'][_]),
                                 sc[source].mean.val['x'][_] + np.sqrt(sc[source].var.val['x'][_])],
                                [sc[source].mean.val['y'][_], sc[source].mean.val['y'][_]],
                                [sc[source].mean.val['z'][_], sc[source].mean.val['z'][_]],
                                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

                        ax.plot([sc[source].mean.val['x'][_], sc[source].mean.val['x'][_]],
                                [sc[source].mean.val['y'][_] - np.sqrt(sc[source].var.val['y'][_]),
                                 sc[source].mean.val['y'][_] + np.sqrt(sc[source].var.val['y'][_])],
                                [sc[source].mean.val['z'][_], sc[source].mean.val['z'][_]],
                                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

                        ax.plot([sc[source].mean.val['x'][_], sc[source].mean.val['x'][_]],
                                [sc[source].mean.val['y'][_], sc[source].mean.val['y'][_]],
                                [sc[source].mean.val['z'][_] - np.sqrt(sc[source].var.val['z'][_]),
                                 sc[source].mean.val['z'][_] + np.sqrt(sc[source].var.val['z'][_])],
                                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

            if oms is not None:
                colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
                for om_id in oms:
                    ax.plot([oms[om_id].pos[0], oms[om_id].pos[0] + oms[om_id].dir[0]],
                            [oms[om_id].pos[1], oms[om_id].pos[1] + oms[om_id].dir[1]],
                            [oms[om_id].pos[2], oms[om_id].pos[2] + oms[om_id].dir[2]],
                            label='om ' + str(om_id),
                            color=next(colors))
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')
            plt.legend()
            plt.show()


def plot_movement_2d(sources, oms, xis, title_prefix, one_plot=False, output_dir=None):
    """
    Plotting top or side view ov movement.

    :param signal:
    :param oms:
    :param H:
    :param xi:
    :param n_pixels:
    :param title_prefix:
    :param output_dir:
    :return:
    """

    if not one_plot:
        for source in sources:
            if isinstance(source, LS.BioSource):
                n_pixels = source.time_domain.shape[0]
                for axis in ['y','z']:

                    plt.figure(figsize=(16,16))
                    plt.title('Movement - x' + axis + ' axis of source: ' + str(source.id))

                    if xis is not None:
                        pos = []
                        for _ in range(n_pixels):
                            for xi in xis:
                                plt.plot(source.pos.force(xi[0]).val['x'][_],
                                        source.pos.force(xi[0]).val[axis][_],
                                        xi[2], alpha=n_pixels / (n_pixels + 2 * _))
                                
                                pos.append([source.pos.force(xi[0]).val['x'][_],
                                            source.pos.force(xi[0]).val['y'][_],
                                            source.pos.force(xi[0]).val['z'][_]])

                        pos = np.mean(np.array(pos),axis=0)

                    if oms is not None:
                        colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
                        for om_id in oms:
                            distance = np.array([oms[om_id].pos[0], oms[om_id].pos[1], oms[om_id].pos[2]]) - pos
                            distance = np.sqrt(np.sum(distance**2))

                            plt.plot([oms[om_id].pos[0], oms[om_id].pos[0] + oms[om_id].dir[0]],
                                    [oms[om_id].pos[['x', 'y', 'z'].index(axis)],
                                    oms[om_id].pos[['x', 'y', 'z'].index(axis)] +
                                    oms[om_id].dir[['x', 'y', 'z'].index(axis)]],
                                    label='OM{0} distance: {1:.2f}'.format(om_id, distance),
                                    color=next(colors))
                    plt.legend()
                    plt.xlabel('x')
                    plt.ylabel(axis)
                    plt.axis('equal')

                    helpers.plot_save(plt, 'x' + axis + str(source.id) + '_' + title_prefix,
                                    output_dir)
    else:
        for axis in ['y','z']:

            plt.figure(figsize=(16,16))
            plt.title('Movement - x' + axis + ' axis')

            for source in sources:
                if isinstance(source, LS.BioSource):
                    n_pixels = source.time_domain.shape[0]
                    pos = []
                    if xis is not None:
                        for _ in range(n_pixels):
                            for xi in xis:
                                plt.plot(source.pos.force(xi[0]).val['x'][_],
                                        source.pos.force(xi[0]).val[axis][_],
                                        xi[2], alpha=n_pixels / (n_pixels + 2 * _))

                                pos.append([source.pos.force(xi[0]).val['x'][_],
                                            source.pos.force(xi[0]).val['y'][_],
                                            source.pos.force(xi[0]).val['z'][_]])

                        pos = np.mean(np.array(pos),axis=0)

                    if oms is not None:
                        colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
                        for om_id in oms:
                            distance = np.array([oms[om_id].pos[0], oms[om_id].pos[1], oms[om_id].pos[2]]) - pos
                            distance = np.sqrt(np.sum(distance**2))
                            plt.plot([oms[om_id].pos[0], oms[om_id].pos[0] + oms[om_id].dir[0]],
                                    [oms[om_id].pos[['x', 'y', 'z'].index(axis)],
                                    oms[om_id].pos[['x', 'y', 'z'].index(axis)] +
                                    oms[om_id].dir[['x', 'y', 'z'].index(axis)]],
                                    label='OM{0} distance: {1:.2f}'.format(om_id, distance),
                                    color=next(colors))
                    plt.legend()
                    plt.xlabel('x')
                    plt.ylabel(axis)
                    plt.axis('equal')

            helpers.plot_save(plt, 'x' + axis + '_' + title_prefix,
                                output_dir)




def plot_movement_3d_error(sc, signal, xi, responses, n_pixels, title):
    """
    Plotting 3D movement with error bars
    :param sc:
    :param signal:
    :param xi:
    :param oms:
    :param title:
    :return:
    """

    colors = iter(om_cmap(np.linspace(0, 1, len(responses))))

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ax = fig.add_subplot(111, projection='3d')
    ax.set_title(title)
    for _ in range(n_pixels):
        ax.scatter(sc['x'].mean.val[_], sc['y'].mean.val[_], sc['z'].mean.val[_],
                   marker='o', alpha=n_pixels / (n_pixels + 2 * _), c='r')

        if xi is not None:
            ax.scatter(signal['x'](xi.extract(signal['x'].domain)).val[_],
                       signal['y'](xi.extract(signal['y'].domain)).val[_],
                       signal['z'](xi.extract(signal['z'].domain)).val[_],
                       marker='o', alpha=n_pixels / (n_pixels + 2 * _), c='k')

        # error
        ax.plot([sc['x'].mean.val[_], sc['x'].mean.val[_]],
                [sc['y'].mean.val[_], sc['y'].mean.val[_]],
                [sc['z'].mean.val[_] - np.sqrt(sc['z'].var.val[_]),
                 sc['z'].mean.val[_] + np.sqrt(sc['z'].var.val[_])],
                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

        ax.plot([sc['x'].mean.val[_] - np.sqrt(sc['x'].var.val[_]),
                 sc['x'].mean.val[_] + np.sqrt(sc['x'].var.val[_])],
                [sc['y'].mean.val[_], sc['y'].mean.val[_]],
                [sc['z'].mean.val[_], sc['z'].mean.val[_]],
                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

        ax.plot([sc['x'].mean.val[_], sc['x'].mean.val[_]],
                [sc['y'].mean.val[_] - np.sqrt(sc['y'].var.val[_]),
                 sc['y'].mean.val[_] + np.sqrt(sc['y'].var.val[_])],
                [sc['z'].mean.val[_], sc['z'].mean.val[_]],
                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

    for r in responses:
        ax.plot([r.pos[0], r.pos[0] + r.dir[0]],
                 [r.pos[1], r.pos[1] + r.dir[1]],
                 [r.pos[2], r.pos[2] + r.dir[2]],
                label='om ' + str(r.id), color=next(colors))

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    plt.show()


def plot_movement_2d_stats8(samples, oms, sources, title_prefix, output_dir):

    for source in sources:

        if isinstance(source, LS.BioSource):
            n_pixels = source.time_domain.shape[0]
            for axis in ['y', 'z']:
                pos = []
                
                colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
                plt.figure(figsize=(16, 16))
                plt.title('Movement - x' + axis + ' axis of source: ' + source.__repr__())

                mean, var = samples.sample_stat(source.signal)

                for _ in range(n_pixels):

                    plt.plot(mean.val['x'][_], mean.val[axis][_],
                             'ro', alpha=n_pixels / (n_pixels + 2 * _))

                    # error
                    plt.plot([mean.val['x'][_] - np.sqrt(var.val['x'][_]),
                              mean.val['x'][_] + np.sqrt(var.val['x'][_])],
                             [mean.val[axis][_], mean.val[axis][_]],
                             'r',
                             alpha=n_pixels / (n_pixels + 2 * _), linestyle='dashed')

                    plt.plot([mean.val['x'][_],
                              mean.val['x'][_]],
                             [mean.val[axis][_] - np.sqrt(var.val[axis][_]),
                              mean.val[axis][_] + np.sqrt(var.val[axis][_])],
                             'r',
                             alpha=n_pixels / (n_pixels + 2 * _), linestyle='dashed')

                    pos.append([mean.val['x'][_],
                                mean.val['y'][_],
                                mean.val['z'][_]])

                pos = np.mean(np.array(pos),axis=0)

                for om_id in oms:
                    i_axis = 1 if axis == 'y' else 2
                    om_dir = np.array([oms[om_id].dir[0], oms[om_id].dir[1], oms[om_id].dir[2]])
                    r = pos - np.array([oms[om_id].pos[0], oms[om_id].pos[1], oms[om_id].pos[2]])
                    distance = np.sqrt(np.sum(r**2))
                    ct = np.dot(om_dir, r / distance)
                    aa = R.ang_accept_single(ct)

                    plt.plot([oms[om_id].pos[0],
                              oms[om_id].pos[0] + oms[om_id].dir[0]],
                             [oms[om_id].pos[i_axis],
                              oms[om_id].pos[i_axis] + oms[om_id].dir[i_axis]],
                              label='OM{0} distance: {1:.2f}, aa: {2:.4f}'.format(om_id, distance, aa),
                              color=next(colors))
                plt.legend()
                plt.xlabel('x')
                plt.ylabel(axis)
                plt.axis('equal')

                helpers.plot_save(plt, 'x' + axis + source.__repr__() + title_prefix,
                                  output_dir)

def plot_movement_2d_error(sc, oms, sources, title_prefix, output_dir):

    for source in sources:

        if isinstance(source, LS.BioSource):
            n_pixels = source.time_domain.shape[0]
            source = source.__repr__()
            for axis in ['y', 'z']:
                pos = []
                
                colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
                plt.figure(figsize=(16, 16))
                plt.title('Movement - x' + axis + ' axis of source: ' + source)
                for _ in range(n_pixels):

                    plt.plot(sc[source].mean.val['x'][_], sc[source].mean.val[axis][_],
                             'ro', alpha=n_pixels / (n_pixels + 2 * _))

                    # error
                    plt.plot([sc[source].mean.val['x'][_] - np.sqrt(sc[source].var.val['x'][_]),
                              sc[source].mean.val['x'][_] + np.sqrt(sc[source].var.val['x'][_])],
                             [sc[source].mean.val[axis][_], sc[source].mean.val[axis][_]],
                             'r',
                             alpha=n_pixels / (n_pixels + 2 * _), linestyle='dashed')

                    plt.plot([sc[source].mean.val['x'][_],
                              sc[source].mean.val['x'][_]],
                             [sc[source].mean.val[axis][_] - np.sqrt(sc[source].var.val[axis][_]),
                              sc[source].mean.val[axis][_] + np.sqrt(sc[source].var.val[axis][_])],
                             'r',
                             alpha=n_pixels / (n_pixels + 2 * _), linestyle='dashed')

                    pos.append([sc[source].mean.val['x'][_],
                                sc[source].mean.val['y'][_],
                                sc[source].mean.val['z'][_]])

                pos = np.mean(np.array(pos),axis=0)

                for om_id in oms:
                    i_axis = 1 if axis == 'y' else 2
                    om_dir = np.array([oms[om_id].dir[0], oms[om_id].dir[1], oms[om_id].dir[2]])
                    r = pos - np.array([oms[om_id].pos[0], oms[om_id].pos[1], oms[om_id].pos[2]])
                    distance = np.sqrt(np.sum(r**2))
                    ct = np.dot(om_dir, r / distance)
                    aa = R.ang_accept_single(ct)

                    plt.plot([oms[om_id].pos[0],
                              oms[om_id].pos[0] + oms[om_id].dir[0]],
                             [oms[om_id].pos[i_axis],
                              oms[om_id].pos[i_axis] + oms[om_id].dir[i_axis]],
                              label='OM{0} distance: {1:.2f}, aa: {2:.4f}'.format(om_id, distance, aa),
                              color=next(colors))
                plt.legend()
                plt.xlabel('x')
                plt.ylabel(axis)
                plt.axis('equal')

                helpers.plot_save(plt, 'x' + axis + source + title_prefix,
                                  output_dir)
