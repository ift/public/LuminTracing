import numpy as np
import nifty6 as ift

###############################################################################
# Math utilities
###############################################################################

def euler_rot(euler, vec):

    x = (euler[0].cos()*euler[2].cos() - euler[0].sin()*euler[1].cos()*euler[2].sin())*vec[0] +\
        (-euler[0].cos()*euler[2].sin() - euler[0].sin()*euler[1].cos()*euler[2].sin())*vec[1] +\
        euler[0].sin()*euler[1].sin()*vec[2]

    y = (euler[0].sin()*euler[2].cos() + euler[0].cos()*euler[1].cos()*euler[2].sin())*vec[0] +\
        (-euler[0].sin()*euler[2].sin() + euler[0].cos()*euler[1].cos()*euler[2].cos())*vec[1] -\
        euler[0].cos()*euler[1].sin()*vec[2]

    z = euler[1].sin()*euler[2].sin()*vec[0] +\
        euler[1].sin()*euler[2].cos()*vec[1] +\
        euler[1].cos()*vec[2]

    return [x,y,z]

def euler_rot_op_field(euler, vec):

    x = (euler[0].cos()*euler[2].cos() - euler[0].sin()*euler[1].cos()*euler[2].sin())(vec[0]) +\
        (-euler[0].cos()*euler[2].sin() - euler[0].sin()*euler[1].cos()*euler[2].sin())(vec[1]) +\
        euler[0].sin()*euler[1].sin()(vec[2])

    y = (euler[0].sin()*euler[2].cos() + euler[0].cos()*euler[1].cos()*euler[2].sin())(vec[0]) +\
        (-euler[0].sin()*euler[2].sin() + euler[0].cos()*euler[1].cos()*euler[2].cos())(vec[1]) +\
        -euler[0].cos()*euler[1].sin()(vec[2])

    z = euler[0].sin()*euler[2].sin()(vec[0]) +\
        euler[1].sin()*euler[2].sin()(vec[1]) +\
        euler[1].cos()(vec[2])

    return [x,y,z]

def real_spherical_harmonics_car(l, m, x, r2):

    if l==0 and m==0:
        # l=0 m=0
        #factor = ift.ScalingOperator(1/(np.sqrt(np.pi)*2), x[0].target[0])
        #factor = ift.Field.full(x[0].target[0], 1/(np.sqrt(np.pi)*2))
        #return factor(x[0]*x[0].one_over())
        return 1/(np.sqrt(np.pi)*2)
    elif l==1 and m==0:
        # l=1 m=-1
        #factor = ift.ScalingOperator(np.sqrt(3/(4*np.pi)), x[0].target[0])
        #return factor(x[1]*r2.sqrt().one_over())
        return np.sqrt(3/(4*np.pi))*x[1]*r2.sqrt().one_over()
    elif l==1 and m==1:
        # l=1 m=0
        #factor = ift.ScalingOperator(np.sqrt(3/(4*np.pi)), x[0].target[0])
        #return factor(x[2]*r2.sqrt().one_over())
        return np.sqrt(3/(4*np.pi))*x[2]*r2.sqrt().one_over()
    elif l==1 and m==2:
        # l=1 m=-1
        #factor = ift.ScalingOperator(np.sqrt(3/(4*np.pi)), x[0].target[0])
        #return factor(x[0]*r2.sqrt().one_over())
        return np.sqrt(3/(4*np.pi))*x[0]*r2.sqrt().one_over()
    elif l==2 and m==0:
        # l=2 m=-2
        #factor = ift.ScalingOperator(0.5*np.sqrt(15/(np.pi)), x[0].target[0])
        #return factor(x[0]*x[1]*r2.one_over())
        return 0.5*np.sqrt(15/(np.pi))*x[0]*x[1]*r2.one_over()
    elif l==2 and m==1:
        # l=2 m=-1
        #factor = ift.ScalingOperator(0.5*np.sqrt(15/(np.pi)), x[0].target[0])
        #return factor(x[1]*x[2]*r2.one_over())
        return 0.5*np.sqrt(15/(np.pi))*x[1]*x[2]*r2.one_over()
    elif l==2 and m==2:
        # l=2 m=0
        #factor = ift.ScalingOperator(0.25*np.sqrt(5/(np.pi)), x[0].target[0])
        #return factor((-x[0]*x[0]-x[1]*x[1]+x[2]*x[2]+x[2]+x[2])*r2.one_over())
        return 0.25*np.sqrt(5/(np.pi))*(-x[0]*x[0]-x[1]*x[1]+x[2]*x[2]+x[2]+x[2])*r2.one_over()
    elif l==2 and m == 3:
        # l=2 m=1
        #factor = ift.ScalingOperator(0.5*np.sqrt(15/ (np.pi)), x[0].target[0])
        #return factor(x[0]*x[2]*r2.one_over())
        return 0.5*np.sqrt(15/ (np.pi))*x[0]*x[2]*r2.one_over()
    elif l==2 and m == 4:
        # l=2 m=2
        #factor = ift.ScalingOperator(0.5*np.sqrt(15/ (np.pi)), x[0].target[0])
        #return factor((x[0]*x[0]-x[1]*x[1])*r2.one_over())
        return 0.5*np.sqrt(15/ (np.pi))*(x[0]*x[0]-x[1]*x[1])*r2.one_over()

def real_spherical_harmonics(l,m, theta, phi):

    if l==0 and m==0:
        # l=0 m=0
        factor = ift.ScalingOperator(1/(np.sqrt(np.pi)*2), theta.target[0])
        factor = ift.Field.full(theta.target[0], 1/(np.sqrt(np.pi)*2))
        return factor
    elif l==1 and m==0:
        # l=1 m=-1
        factor = ift.ScalingOperator(np.sqrt(3/(4*np.pi)), theta.target[0])
        return factor(theta.sin()*phi.sin())
    elif l==1 and m==1:
        # l=1 m=0
        factor = ift.ScalingOperator(np.sqrt(3/(4*np.pi)), theta.target[0])
        return factor(theta.cos())
    elif l==1 and m==2:
        # l=1 m=-1
        factor = ift.ScalingOperator(np.sqrt(3/(4*np.pi)), theta.target[0])
        return factor(theta.sin()*phi.cos())
    elif l==2 and m==0:
        # l=2 m=-2
        factor = ift.ScalingOperator(0.5*np.sqrt(15/(np.pi)), theta.target[0])
        return factor(theta.sin()*theta.sin()*phi.sin()*phi.cos())
    elif l==2 and m==1:
        # l=2 m=-1
        factor = ift.ScalingOperator(0.5*np.sqrt(15/(np.pi)), theta.target[0])
        return factor(theta.cos()*theta.sin()*phi.cos())
    elif l==2 and m==2:
        # l=2 m=0
        factor = ift.ScalingOperator(0.75*np.sqrt(5/(np.pi)), theta.target[0])
        adder = ift.Adder(ift.Field.full(0.25*np.sqrt(5/(np.pi)), theta.target[0]), neg=True)
        return adder(factor(theta.cos()*theta.cos()))
    elif l==2 and m == 3:
        # l=2 m=1
        factor = ift.ScalingOperator(0.5*np.sqrt(15/ (np.pi)), theta.target[0])
        return factor(theta.cos()*theta.sin()*phi.cos())
    elif l==2 and m == 4:
        # l=2 m=2
        factor = ift.ScalingOperator(0.5*np.sqrt(15/ (np.pi)), theta.target[0])
        return factor(theta.sin()*theta.sin() * (phi+phi).cos())

def sph2car_field(r=None, theta=None, phi=None):

    if r is None:
        x = theta.sin() * phi.cos()
        y = theta.sin() * phi.sin()
        z = theta.cos()

    return [x,y,z]

def rot_mat(x,y,z, alpha,axis):
    cos_alpha = ift.ScalingOperator(np.cos(alpha), x.target[0])
    sin_alpha = ift.ScalingOperator(np.sin(alpha), x.target[0])

    if axis=='x':
        x = x
        y = cos_alpha(y) - sin_alpha(z)
        z = sin_alpha(y) + cos_alpha(z)

        return x,y,z

def sph2car(r, theta, phi):
    return np.array([r * np.sin(theta) * np.cos(phi),
                     r * np.sin(theta) * np.sin(phi),
                     r * np.cos(theta)])

def dot_field(x,y):
    return x[0]*y[0] + x[1]*y[1] + x[2]*y[2]

def dot_op_field(op, field):
    return op[0](field[0]) + op[1](field[1]) + op[2](field[2])