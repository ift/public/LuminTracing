import numpy as np
import nifty6 as ift
import os
import pickle
import time
from . import helpers
from . import plot


def get_H(responses, data, ic_sampling, output_dir):
    """
    Calculating likelihood and information Hamiltonian.

    :param responses:
    :param data:
    :param ic_sampling:
    :param output_dir:
    :return:
    """

    start_time = time.time()

    # Likelihood
    likelihood_list = []
    for om_id in responses:
        for n in range(len(responses[om_id])):
            likelihood_list.append(
                ift.PoissonianEnergy(data[om_id][n])(responses[om_id][n].mask @
                                                     responses[om_id][n].response))

    likelihood = ift.utilities.my_sum(likelihood_list)

    # information Hamiltonian
    H = ift.StandardHamiltonian(likelihood, ic_sampling)

    # Saving
    helpers.save(H, 'H.pickle', output_dir)

    end_time = time.time() - start_time

    return H, likelihood, end_time

def minimize_KL(sources, oms, responses,
                data, domains,
                H, H_init,
                minimizer, constants,
                n_samples, sample_mod, sample_increase,
                n_global_iterations,
                n_local_iterations, local_mod, local_increase, iteration_limit,
                seed, comm=None, output_dir=None, plot_flag=False):
    """

    :param x:
    :param y:
    :param z:
    :param oms:
    :param mock_xi:
    :param mock_data:
    :param H:
    :param H_init:
    :param minimizer:
    :param n_samples:
    :param n_global_iterations:
    :param iteration_limit:
    :param n_local_iterations:
    :param output_dir:
    :return:
    """

    start_time = time.time()
    ift.random.push_sseq_from_seed(seed)
    g = 0
    l = 0
    energy_loc = 0
    energy_glob = 0

    l_list = []
    n_samples_list = []


    if comm is None:
        rank = 0
    else:
        rank = comm.Get_rank()

    if minimizer is None:
        ic_newton = ift.AbsDeltaEnergyController(deltaE=1e-8, iteration_limit=iteration_limit)

        minimizer = ift.NewtonCG(ic_newton)


    for g in range(n_global_iterations):

        
        print("\nGlobal Interation: " + str(g+1))

        # Draw new samples and minimize KL
        if np.mod(g,sample_mod) == 0:
            n_samples = n_samples+sample_increase
        if np.mod(g,local_mod) == 0:
            n_local_iterations = n_local_iterations+local_increase

        n_samples_list.append(n_samples)
        l_list.append(n_local_iterations)

        KL = ift.MetricGaussianKL(H_init, H, n_samples, constants=constants, mirror_samples=True, comm=comm)

        for l in range(n_local_iterations):

            KL, convergence = minimizer(KL)
            H_init = KL.position

            energy_tmp = H(KL.position).val
            energy_diff = (energy_loc - energy_tmp)/(energy_tmp + 1e-8)
            energy_loc = energy_tmp

            print('\tLocal iteration ' + str(l+1) + ' with ' + str(iteration_limit) + ' steps.')
            print('\t\tH: ' + str(energy_loc) + '\tDelta H:  ' + str(energy_diff))

        print('Total iteration steps: ' + str((l+1) * iteration_limit))
        print('\tH: ' + str(energy_loc) + '\tDelta H:  ' + str((energy_glob - energy_loc)/(energy_loc+ 1e-8)))
        energy_glob = energy_loc

        if plot_flag and rank==0:
            for i in range(len(domains)):
                plot.plot_data(responses, xis=[(KL.position, '/recon', None)], data=data, dataset=i,
                               title_prefix=str(i) + 's' + str(n_samples) + 'g' + str(g) + 'l' + str(l),
                               output_dir=output_dir + '/data')

            plot.plot_signal(sources, oms, responses, xis=[(KL.position, '/recon', 'ro')],
                             title_prefix='s' + str(n_samples) + 'g' + str(g) + 'l' + str(l),
                             output_dir=output_dir + '/signal')

            plot.plot_movement_2d(sources, oms, xis=[(KL.position, '/recon', 'ro')],
                                  title_prefix='s' + str(n_samples) + 'g' + str(g) + 'l' + str(l),
                                  output_dir=output_dir + '/movement')

        # Saving
        if output_dir is not None and rank==0:
            # Creating output dir
            print('Saving KL.pickle')
            helpers.save(KL.position, 'KL_' + str(g) + '_' + str(l) + '.pickle', output_dir)

    end_time = time.time() - start_time

    return KL, (g, l_list, n_samples_list), end_time

def get_stat_calcs(minimized_obj, sources, oms, responses, output_dir):
    """
    Calculate stats with samples.

    :param n_samples:
    :param signal:
    :param minimized_obj:
    :param output_dir:
    :return:
    """

    start_time = time.time()

    # Draw posterior samples
    if len(minimized_obj)==3:
        KL = ift.MetricGaussianKL(minimized_obj[0], minimized_obj[1], minimized_obj[2])
    else:
        KL = minimized_obj[0]

    # l_att samples
    l_att_samples = []
    for i in range(minimized_obj[2]):
        l_att_sample = []
        for om_id in oms:
            l_att_sample.append(ift.from_random(oms[om_id].l_att.domain))
        l_att_samples.append(ift.MultiField.union(l_att_sample))


    sc = {}
    sc['KL'] = ift.StatCalculator()
    for i, sample in enumerate(KL.samples):
        #sample = ift.MultiField.union([sample, l_att_samples[i]])
        sc['KL'].add(sample + KL.position)

    sc['H'] = ift.StatCalculator()
    for i, sample in enumerate(KL.samples):
        #sample = ift.MultiField.union([sample, l_att_samples[i]])
        sc['H'].add(minimized_obj[1](sample + KL.position))

    if sources is not None:
        for source in sources:
            #sample = ift.MultiField.union([sample, l_att_samples[i]])
            sc[source.__repr__()] = ift.StatCalculator()

        for i, sample in enumerate(KL.samples):
            for source in sources:
                #sample = ift.MultiField.union([sample, l_att_samples[i]])
                sc[source.__repr__()].add(source.signal.force(sample + KL.position))

    if oms is not None:
        for om_id in oms:
            sc[oms[om_id].__repr__()] = ift.StatCalculator()

        for i, sample in enumerate(KL.samples):
            for om_id in oms:
                signal = oms[om_id].signal
                #sample = ift.MultiField.union([sample, l_att_samples[i]])
                sc[oms[om_id].__repr__()].add(signal.force(sample + KL.position))

    if responses is not None:
        for om_id in responses:
            for n in range(len(responses[om_id])):
                sc[responses[om_id][n].__repr__()] = ift.StatCalculator()

        for i, sample in enumerate(KL.samples):
            for om_id in responses:
                for n in range(len(responses[om_id])):
                    signal = responses[om_id][n].signal
                    #sample = ift.MultiField.union([sample, l_att_samples[i]])
                    sc[responses[om_id][n].__repr__()].add(signal.force(sample + KL.position))


    # Saving
    helpers.save(sc, 'sc.pickle', output_dir)

    end_time = time.time() - start_time

    return sc, end_time
