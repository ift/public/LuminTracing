import nifty6 as ift
import numpy as np

###############################################################################
# NIFTy Operator
###############################################################################

class Threshold(ift.Operator):
    """
    Threshold operator.
    Below the threshold values go to 0.
    """
    def __init__(self, domain, threshold):
        self._domain = domain
        self._target = domain
        self._threshold = threshold

    def apply(self, x):
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        if not lin:
            idx = x.val < self._threshold
            clipped = x.val.copy()
            clipped[idx] = 1
            return ift.Field(self.target, clipped)

        idx = x.val.val < self._threshold
        clipped = x.val.val.copy()
        clipped[idx] = 1
        v = ift.Field(self.target, clipped)
        return x.new(v, ift.makeOp(ift.Field(self.target, ~idx))(x.jac))

def init_mask_op(target, mask):
    # Masking operator to model that parts of the field have not been observed
    mask = ift.Field.from_raw(target, mask)
    mask = ift.MaskOperator(mask)

    return mask

class Uniformizer(ift.Operator):
    """

    """
    def __init__(self, domain, min, max):
        self._domain = domain
        self._target = domain
        self._min = min
        self._max = max
        self._vol = max - min

    def apply(self, x):
        import scipy.stats as st
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        if not lin:
            return ift.Field(self.target, st.norm.cdf(x.val)*self._vol + self._min)
        v = ift.Field(self.target, st.norm.cdf(x.val.val)*self._vol + self._min)
        return x.new(v, ift.makeOp(ift.Field(self.target, st.norm.pdf(x.val.val)*self._vol))(x.jac))


class TimeMaskOperator(ift.LinearOperator):

    """Implementation of a mask response

    Takes a field, applies flags and returns the values of the field in a
    :class:`UnstructuredDomain`.

    Parameters
    ----------
    flags : Field
        Is converted to boolean. Where True, the input field is flagged.
    """
    def __init__(self, flags, domain):
        if not isinstance(flags, ift.Field):
            raise TypeError
        self._domain = ift.DomainTuple.make(flags.domain)
        self._flags = np.logical_not(flags.val)
        self._target = ift.DomainTuple.make(domain)
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        x = x.val
        if mode == self.TIMES:
            res = x[self._flags]
            return ift.Field.from_raw(self.target, res)
        res = np.empty(self.domain.shape, x.dtype)
        res[self._flags] = x
        res[~self._flags] = 0
        return ift.Field.from_raw(self.domain, res)


class AngAcceptOperator(ift.Operator):
    def __init__(self, domain):
        self._domain = domain
        self._target = domain
        self._a = [0.3265, 0.6144, -0.0343, -0.0641, 0.2988, -0.1422]

    def apply(self, x):
        self._check_input(x)
        lin = isinstance(x, ift.Linearization)
        if not lin:
            y = self._a[0] + \
                self._a[1] * x.val + \
                self._a[2] * x.val ** 2 + \
                self._a[3] * x.val ** 3 + \
                self._a[4] * x.val ** 4 + \
                self._a[5] * x.val ** 5
            return ift.Field(self._target, y)

        y = self._a[0] + \
            self._a[1] * x.val.val + \
            self._a[2] * x.val.val ** 2 + \
            self._a[3] * x.val.val ** 3 + \
            self._a[4] * x.val.val ** 4 + \
            self._a[5] * x.val.val ** 5

        dy = self._a[1] + \
             2 * self._a[2] * x.val.val + \
             3 * self._a[3] * x.val.val ** 2 + \
             4 * self._a[4] * x.val.val ** 3 + \
             5 * self._a[5] * x.val.val ** 4

        v = ift.Field(self._target, y)
        return x.new(v, ift.makeOp(ift.Field(self._target, dy))(x.jac))