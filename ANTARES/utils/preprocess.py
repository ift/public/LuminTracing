import numpy as np
import nifty6 as ift
from . import data as data_utils
from ..models.Response import Response
import time
from . import helpers

###############################################################################
#  Mask Creation
###############################################################################


def create_mask(data, flag):

    if flag == 'signal':
        idx = (data < 500)
        mask_arr = np.convolve(idx, np.ones(3), mode='same') > 0

    elif flag == 'burst':
        idx = (data < 5500)
        mask_arr = np.convolve(idx, np.ones(3), mode='same') > 0

    elif flag == 'bkg':
        # bkg mask
        idx1 = (data < 100)
        idx2 = (data > 10000)
        idx3 = np.append(np.diff(data) > 1000, 1)
        mask1 = np.convolve(idx1, np.ones(10), mode='same') > 0
        mask2 = np.convolve(idx2, np.ones(10), mode='same') > 0
        mask3 = np.convolve(idx3, np.ones(10), mode='same') > 0
        mask_arr = (mask1 + mask2 + mask3) > 0

    elif flag == 'no_mask':
        mask_arr = (np.zeros_like(data) > 1)

    return mask_arr

###################################################################################################
# Create synthetic data
###################################################################################################


def create_mock_data(sources, oms, output_dir=None):
    """

    :param responses:
    :param output_dir:
    :return:
    """

    start_time = time.time()

    mock_xi = []
    mock_data = {}
    ground_truth = {}
    for source in sources:
        mock_xi.append(ift.from_random('normal', source.signal.domain))
    for om_id in oms:
        mock_xi.append(ift.from_random('normal', oms[om_id].eff.domain))

    mock_xi = ift.MultiField.union(mock_xi)

    for om_id in oms:
        ground_truth[om_id] = oms[om_id].response.force(mock_xi)
        _ = np.random.poisson(ground_truth[om_id].val).astype('int')
        mock_data[om_id] = (ift.makeField(oms[om_id].response.target[0], _))

    helpers.save([mock_xi, mock_data, ground_truth], 'mock.pickle', output_dir)

    end_time = time.time() - start_time
    print("create_mock_data \tComputation time: " + str(end_time * 1000) + str(" ms"))

    return mock_xi, mock_data, ground_truth

###############################################################################
#  Data and Response
###############################################################################


def create_data_response(DATA_FILE, EFF_FILE, OM_ID, DATA_SAMPLE, CUSTOM_MASK, mirror_data=False):

    data = {}
    responses = {}
    domains = []

    df, dt, om_info, _ = data_utils.prepare_data(DATA_FILE, EFF_FILE, OM_ID)

    delete_oms = []

    for n in range(len(DATA_SAMPLE)):

        N_PIXELS = DATA_SAMPLE[n][1] - DATA_SAMPLE[n][0]

        time_space = ift.RGSpace(N_PIXELS, dt)
        time_domain = ift.DomainTuple.make(time_space)
        domains.append(time_domain)

        for i, om_id in enumerate(om_info):
            if n == 0:
                data[om_id] = []
                responses[om_id] = []

            # data
            tmp = df[om_id[0]][om_id[1]][DATA_SAMPLE[n][0]:DATA_SAMPLE[n][1]]
            if mirror_data:
                tmp = np.flip(tmp)

            mask_arr = create_mask(tmp, DATA_SAMPLE[n][2])
            if CUSTOM_MASK is not None:
                mask_arr = ((CUSTOM_MASK[om_id] + mask_arr) >0)
            if np.sum(~mask_arr) <= N_PIXELS*0.2:
                print('delete')
                delete_oms.append(om_id)
            else:
                r = Response(str(om_id) + DATA_SAMPLE[n][2], mask_arr, time_domain)

                data[om_id].append(r.mask(ift.makeField(r.mask.domain,
                                                       np.round(tmp).astype('int'))))
                responses[om_id].append(r)

    for key in set(delete_oms):
        om_info.pop(key)
        data.pop(key)
        responses.pop(key)

    return data, responses, om_info, domains, set(delete_oms)

###################################################################################################
# Call Responses
###################################################################################################


def call_responses(oms, source_dict, responses, om_info, output_dir=None):

    for om_id in om_info:
        for i, s in enumerate(source_dict[om_id]):
            responses[om_id][i](oms[om_id], s)

    helpers.save(responses, 'responses.pickle', output_dir)

    return responses

