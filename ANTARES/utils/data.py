import numpy as np
import pandas as pd
import nifty6 as ift
import csv
from ANTARES.models import config


###############################################################################
# Data tools - Pandas
###############################################################################


def str2ns(data):
    """
    String from dataset to nanoseconds.

    :param data:
    :return:
    """

    for i in range(len(data)):
        tmp = data[i][-16:]
        tmp = float(tmp.split(':')[0])*60 + float(tmp.split(':')[1])
        data[i]=tmp
    return (data - data[0]).astype('float')


def load_sort(filename):
    """
    Load data from panda dataframe and sort.

    :param filename:
    :return:
    """

    df = pd.read_csv(filename, index_col=0)
    df.times = pd.to_datetime(df.times, format="%Y-%m-%d %H:%M:%S.%f")
    df = df.sort_values(by=['times', 'om_line', 'om_key'])

    return df


def extract_om(df, line, om):
    """
    Extracting data from dataframe for given optical module.

    :param df:
    :param line:
    :param om:
    :return:
    """

    tmp = df[df['om_line'] == line]
    tmp = tmp[tmp['om_key'] == om]

    # Duplicates due to ?
    tmp = tmp.loc[~tmp.times.duplicated(keep='first')]

    return tmp


def get_dt(times):

    # check if equally distributed
    delta = np.diff(times)
    if all(delta == delta[0]*np.ones(shape=np.shape(times))):
        return delta[0]

    else:
        print("ERROR")


def prepare_data(filename, eff_file, om_list):
    """
    Loading rates and OM info.

    :param filename:
    :param eff_file:
    :param om_list: (l, om)
    :return:
    """

    np_om_list = np.array(om_list)
    df = load_sort(filename)
    eff_dict = eff2dict(eff_file)
    rates = pd.DataFrame(None, columns=np.unique(np_om_list[:,0]),
                         index=np.unique(np_om_list[:,1]))
    om_info = {}

    dt_list = []
    start_flag = True
    for om in om_list:
        # Origin calc
        if start_flag:
            tmp = extract_om(df, om[0], om[1])
            om1 = np.array([tmp['omPosX'].to_numpy()[0],
                      tmp['omPosY'].to_numpy()[0],
                      tmp['omPosZ'].to_numpy()[0]])
            tmp = extract_om(df, om[0], om[1]+1)
            om2 = np.array([tmp['omPosX'].to_numpy()[0],
                   tmp['omPosY'].to_numpy()[0],
                   tmp['omPosZ'].to_numpy()[0]])
            tmp = extract_om(df, om[0], om[1]+2)
            om3 = np.array([tmp['omPosX'].to_numpy()[0],
                   tmp['omPosY'].to_numpy()[0],
                   tmp['omPosZ'].to_numpy()[0]])

            origin = om1 + 0.5 * (om2 - om1) + 0.5 * (om3 - om1 - 0.5 * (om2 - om1))
            #origin = np.array([0,0,0])
            start_flag=False

        tmp = extract_om(df, om[0], om[1])

        # Still hardcoded but okay
        dt = 0.10485760000000255

        rates[om[0]][om[1]] = tmp['rates'].to_numpy()*1e3*dt+1
        om_info[om] = ([tmp['omPosX'].to_numpy()[0],
                             tmp['omPosY'].to_numpy()[0],
                             tmp['omPosZ'].to_numpy()[0]],
                            [tmp['omDirX'].to_numpy()[0],
                            tmp['omDirY'].to_numpy()[0],
                            tmp['omDirZ'].to_numpy()[0]],
                       eff_dict[om])

    return rates, dt, om_info, origin


def get_info(filename, om_list):
    """
    Extract only info  of optical modules.

    :param filename:
    :param om_list: (l, om)
    :return: ((l, om), [pos x, pos y, pos], [dir x, dir y, dir z])
    """

    df = load_sort(filename)
    om_info = []

    for om in om_list:
        tmp = extract_om(df, om[0], om[1])
        om_info.append((om, [tmp['omPosX'].to_numpy()[0],
                             tmp['omPosY'].to_numpy()[0],
                             tmp['omPosZ'].to_numpy()[0]],
                            [tmp['omDirX'].to_numpy()[0],
                            tmp['omDirY'].to_numpy()[0],
                            tmp['omDirZ'].to_numpy()[0]]))

    return om_info


def eff2dict(filename):
    """
    Converting efficiency table to dict.
    :param filename:
    :return:
    """

    dict = {}
    with open(filename, 'r') as file:
        for i, line in enumerate(file):
            key = int(line.split(' ')[1])
            item = float(line.split(' ')[-1][:-2])
            om = float(line.split(' ')[2])
            om_tuple = config.LCMID_DICT[key]
            om_tuple = (om_tuple[0], (om_tuple[1]-1)*3+om+1)
            dict[om_tuple] = item

    return dict






