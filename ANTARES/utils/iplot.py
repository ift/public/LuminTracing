import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from ANTARES.models import LightSources as LS
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)

# define detector color map
om_cmap = plt.get_cmap('nipy_spectral')
s_cmap = plt.get_cmap('viridis')
source_cmap = plt.get_cmap('Dark2')
def set_size(subplots, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = 361 * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * subplots

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim

def plot_data(responses, xis, data, dataset, log_flag=False, figsize=(16,10)):
    """
    Plotting OMS data (Ground Truth, Mock data, reconstruction).

    :param oms:
    :param H:
    :param mock_xi:
    :param mock_data:
    :param title_prefix:
    :param output_dir:
    :return:
    """

    # fix colors of detectors
    om_list = [42, 43, 44, 48,49,50]
    colors = iter(om_cmap(np.linspace(0, 1, len(responses))))

    # fig size
    fig = plt.figure(figsize=figsize, dpi=140)
    plt.title(r"Simulated data $\lambda$")



    for om_id in responses:

        if True: 
            c = next(colors)
            # Ground Truth
            if xis is not None:
                for xi in xis:
                    lumin = responses[om_id][dataset].response.force(xi[0]).val/0.1
                    t = np.linspace(0, (len(lumin)-1)*0.1, len(lumin))
                    if log_flag:
                        
                        plt.plot(t, np.log(lumin),
                                label='om ' + str(om_id) + xi[1],
                                color=c, linestyle='dashed')
                    else:
                        plt.plot(t, lumin,
                                label='om ' + str(om_id) + xi[1],
                                color=c, linestyle='dashed')
            # Mock data
            if data is not None:
                lumin = responses[om_id][dataset].mask.adjoint(data[om_id][dataset]).val/0.1
                t = np.linspace(0, (len(lumin)-1)*0.1, len(lumin))
                if log_flag:
                    plt.plot(t, np.log(lumin),
                            label='om '+str(om_id)+'/data', color=c)
                else:
                    plt.plot(t, lumin,
                            label='om '+str(om_id)+'/data', color=c)


    plt.grid(b=True, which='major', color='#999999', linestyle='-', alpha=0.2)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.xlim((np.min(t), np.max(t)))
    plt.xlabel('Time steps in s')
    plt.ylabel('Photon hit rate in Hz')
    plt.legend(loc='left')
    plt.tight_layout()

def plot_compare_response(data, sc, responses_list, c_label, label, om_list=None, std_flag=False, log_flag=False):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """
    # fix colors of detectors

    if responses_list is not None:
        
        fig, (ax_response, ax_error) = plt.subplots(2, 1, sharex=True, figsize=(9,4))
        colors = source_cmap(np.linspace(0, 0.9, 8))
        
        for i, responses in enumerate(responses_list):
            
            c = colors[c_label[i]]
            
            for om_id in responses:
                if om_id[1] in om_list or om_list is None:

                    for dataset in range(len(responses[om_id])):

                        key = 'response'
                        source = responses[om_id][dataset].__repr__()


                        # Error plot
                        ax_error.set_title("Relative residuals of response - OM" + str(om_id))
                        d = responses[om_id][dataset].mask.adjoint(data[om_id][dataset]).val
                        r = sc[i][source].mean.val[key]
                        idx = d > 2
                        error = np.abs(d - r)
                        sigma = np.sqrt(r)

                        t = np.linspace(0, (len(error)-1)*0.1, len(error))

                        ax_error.plot(t, error/sigma,
                                    label='mean: {0:.2f} median {1:.2f}'.format(np.nanmean(error[idx]/sigma[idx]), np.nanmedian(error[idx]/sigma[idx])), color=c)

                        if i==1:
                            ax_error.hlines(1, xmin=0, xmax=t[-1], colors='r',
                                    linestyles='solid', label=r'$\|d - \lambda|/\sigma =1$')
                        #ax_error.hlines(2, xmin=0, xmax=len(error), colors='r',
                        #            linestyles='dashed', label=r'$\sigma =2$')
                        ax_error.set_ylim(0, 5)
                        ax_error.set_ylabel(r'Relative residuals in units of $\sigma$')

                        # Response plot
                        ax_response.set_title("Reconstruction with different random seeds of response - OM" + str(om_id))

                        if log_flag:
                            if i ==0:
                                ax_response.plot(t, np.log(d/0.1),
                                    label='om ' + str(om_id) + '/data', color='k')
                            # mean
                            ax_response.plot(t, np.log(r/0.1), color=c, label='om ' + str(om_id) + '/recon:' + label[i])

                            # error
                            if std_flag:
                                ax_response.fill_between(t, np.log((r-np.sqrt(sc[i][source].var.val[key]))/0.1),
                                                        np.log((r+np.sqrt(sc[i][source].var.val[key]))/0.1),
                                                        color='lightgray', label='std of recon')
                            # ax_response.plot(t, np.log(r - np.sqrt(sc[source].var.val[key])),
                            #         'r+', label='om ' + str(om_id) + '/recon_std')
                            # ax_response.plot(t, np.log(r + np.sqrt(sc[source].var.val[key])),
                            #         'r+')
                            ax_response.set_ylabel(r'ln($\lambda}$)')

                        else:
                            if i ==0:
                                ax_response.plot(t, d/0.1,
                                    label='om ' + str(om_id) + '/data', color='k')

                            # mean
                            ax_response.plot(t, r/0.1, color=c, label='om ' + str(om_id) + '/recon:' + label[i])

                            # error
                            # ax_response.plot(t, (r - np.sqrt(sc[source].var.val[key]))/0.1,
                            #         'r+', label='om ' + str(om_id) + '/recon_std')
                            # ax_response.plot(t, (r + np.sqrt(sc[source].var.val[key]))/0.1,
                            #         'r+')
                            if std_flag:
                                ax_response.fill_between(t, (r-np.sqrt(sc[source].var.val[key]))/0.1,
                                                        (r+np.sqrt(sc[source].var.val[key]))/0.1,
                                                        color='lightgray', label='std of recon')
                            ax_response.set_ylabel('Photon hit rate in Hz')

                        ax_error.set_xlabel('Time steps in s')
                        ax_response.legend(loc='upper right')
                        ax_error.legend(loc='upper right')
                        ax_error.grid(b=True, which='major', color='#999999', linestyle='-', alpha=0.2)
                        ax_error.minorticks_on()
                        ax_error.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
                        ax_error.set_xlim((np.min(t), np.max(t)))
                        ax_response.legend(loc='upper right')
                        ax_response.grid(b=True, which='major', color='#999999', linestyle='-', alpha=0.2)
                        ax_response.minorticks_on()
                        ax_response.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
                        ax_response.set_xlim((np.min(t), np.max(t)))

                        plt.legend(loc='upper right')

def plot_more_residuals_stats(data, sc, responses, figsize, om_list=None, std_flag=False, log_flag=False, labels=None, styles=None, colors=None):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """
    # fix colors of detectors


    dt = 0.1048576
    for om_id in responses[0]:
        if om_id[1] in om_list or om_list is None:
            
            fig, ax_error = plt.subplots(1, 1, sharex=True, figsize=figsize)
            ax_error.set_title("Relative residuals of response - OM" + str(om_id))


            for i in range(len(labels)):
    
                for dataset in range(len(responses[i][om_id])):

                    key = 'response'
                    source = responses[i][om_id][dataset].__repr__()


                    # Error plot
                    d = responses[i][om_id][dataset].mask.adjoint(data[i][om_id][dataset]).val
                    r = sc[i][source].mean.val[key]
                    idx = d > 2
                    error = d - r
                    sigma = np.sqrt(r)

                    t = np.linspace(0, (len(error)-1)*dt, len(error))

                    ax_error.plot(t, error/sigma,
                                #label='mean: {0:.2f}'.format(np.nanmean(error[idx]/sigma[idx])), # median {1:.2f}'.format(np.nanmean(error[idx]/sigma[idx]), np.nanmedian(error[idx]/sigma[idx])), 
                                color=colors[i], linewidth=0.5, label=labels[i], linestyle=styles[i])

            #ax_error.hlines(1, xmin=0, xmax=t[-1], colors='r',
            #            linestyles='dashed', label=r'$\|d - \lambda\|/\sigma = 1$')
            ax_error.hlines(2, xmin=0, xmax=len(error), colors='r', linewidth=0.5,
                        linestyle='solid', label=r'$|d - \lambda|/\sigma = 2$')
            ax_error.hlines(-2, xmin=0, xmax=len(error), colors='r', linewidth=0.5,
                        linestyle='solid')
            ax_error.set_ylim(-3.5, 4.0)
            ax_error.set_ylabel(r'Relative residuals in units of $\sigma$')

            ax_error.set_xlabel('Time in s')
            ax_error.legend(loc='lower left')
            #ax_error.grid(b=True, which='major', color='#999999', linestyle='-', alpha=0.2)
            ax_error.minorticks_on()
            #ax_error.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
            ax_error.set_xlim((np.min(t), np.max(t)))

            ax_error.xaxis.set_major_locator(MultipleLocator(1))
            ax_error.xaxis.set_minor_locator(MultipleLocator(0.5))
            ax_error.yaxis.set_major_locator(MultipleLocator(1))
            ax_error.yaxis.set_minor_locator(MultipleLocator(0.5))
            ax_error.grid(which='major', color='w', linestyle='-', linewidth=1)
            ax_error.grid(which='minor', color='w', linestyle='-', linewidth=0.5)

            plt.legend(loc='upper left')

def plot_residuals_stats(data, sc, responses, figsize, om_list=None, std_flag=False, log_flag=False):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """
    # fix colors of detectors

    colors = iter(om_cmap(np.linspace(0, 1, len(responses))))
    for om_id in responses:
        c = next(colors)
        if om_id[1] in om_list or om_list is None:

    
            for dataset in range(len(responses[om_id])):

                key = 'response'
                source = responses[om_id][dataset].__repr__()

                fig, ax_error = plt.subplots(1, 1, sharex=True, figsize=figsize)

                # Error plot
                ax_error.set_title("Relative residuals of response - OM" + str(om_id))
                d = responses[om_id][dataset].mask.adjoint(data[om_id][dataset]).val
                r = sc[source].mean.val[key]
                idx = d > 2
                error = np.abs(d - r)
                sigma = np.sqrt(r)

                t = np.linspace(0, (len(error)-1)*0.1, len(error))

                ax_error.plot(t, error/sigma,
                            #label='mean: {0:.2f}'.format(np.nanmean(error[idx]/sigma[idx])), # median {1:.2f}'.format(np.nanmean(error[idx]/sigma[idx]), np.nanmedian(error[idx]/sigma[idx])), 
                            color='k', linewidth=0.5)

                ax_error.hlines(1, xmin=0, xmax=t[-1], colors='r',
                            linestyles='solid', label=r'$\|d - \lambda\|/\sigma = 1$')
                ax_error.hlines(2, xmin=0, xmax=len(error), colors='r',
                            linestyles='dashed', label=r'$\|d - \lambda\|/\sigma = 2$')
                ax_error.set_ylim(0, 5)
                ax_error.set_ylabel(r'Relative residuals in units of $\sigma$')

                ax_error.set_xlabel('Time steps in s')
                ax_error.legend(loc='upper right')
                #ax_error.grid(b=True, which='major', color='#999999', linestyle='-', alpha=0.2)
                ax_error.minorticks_on()
                #ax_error.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
                ax_error.set_xlim((np.min(t), np.max(t)))

                plt.legend(loc='upper right')

def plot_response_stats(data, sc, responses, figsize, om_list=None, std_flag=False, log_flag=False):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """
    # fix colors of detectors

    dt = 0.1048576

    colors = iter(om_cmap(np.linspace(0, 1, len(responses))))
    for om_id in responses:
        c = next(colors)
        if om_id[1] in om_list or om_list is None:

    
            for dataset in range(len(responses[om_id])):

                key = 'response'
                source = responses[om_id][dataset].__repr__()

                fig, ax_response = plt.subplots(1, 1, sharex=True, figsize=figsize)

                # Error plot
                d = responses[om_id][dataset].mask.adjoint(data[om_id][dataset]).val
                r = sc[source].mean.val[key]
                idx = d > 2
                error = np.abs(d - r)
                sigma = np.sqrt(r)

                t = np.linspace(0, (len(error)-1)*dt, len(error))

                # Response plot
                ax_response.set_title("Reconstruction of response - OM" + str(om_id))

                if log_flag:
                    ax_response.plot(t, np.log(d/dt),
                            label='om ' + str(om_id) + r'/data:$d$', color=c, linewidth=0.5)
                    # mean
                    ax_response.plot(t, np.log(r/dt), '.', color='k', label='om ' + str(om_id) + r'/recon:$\lambda$',
                                    markersize=1.0)
                    

                    # error
                    if std_flag:
                        ax_response.fill_between(t, np.log((r-np.sqrt(sc[source].var.val[key]))/0.1),
                                                np.log((r+np.sqrt(sc[source].var.val[key]))/0.1),
                                                color='lightgray', label='std of recon')
                    # ax_response.plot(t, np.log(r - np.sqrt(sc[source].var.val[key])),
                    #         'r+', label='om ' + str(om_id) + '/recon_std')
                    # ax_response.plot(t, np.log(r + np.sqrt(sc[source].var.val[key])),
                    #         'r+')
                    ax_response.set_ylabel(r'ln(lambda)')

                else:
                    ax_response.plot(t, d/dt/1e3,
                            label='om ' + str(om_id) + r'/data:$d$', color=c, linewidth=0.5)

                    # mean
                    ax_response.plot(t, r/dt/1e3,'.', color='k', label='om ' + str(om_id) + r'/recon:$\lambda$', markersize=1.0)

                    # error
                    # ax_response.plot(t, (r - np.sqrt(sc[source].var.val[key]))/0.1,
                    #         'r+', label='om ' + str(om_id) + '/recon_std')
                    # ax_response.plot(t, (r + np.sqrt(sc[source].var.val[key]))/0.1,
                    #         'r+')
                    if std_flag:
                        ax_response.fill_between(t, (r-np.sqrt(sc[source].var.val[key]))/dt/1e3,
                                                (r+np.sqrt(sc[source].var.val[key]))/dt/1e3,
                                                color='lightgray', label='std of recon')
                    ax_response.set_ylabel('Photon hit rate in kHz')

                ax_response.set_xlabel('Time in s')
                ax_response.legend(loc='upper right')
                ax_response.legend(loc='upper right')
                #ax_response.grid(b=True, which='major', color='#999999', linestyle='-', alpha=0.2)
                ax_response.minorticks_on()
                #ax_response.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
                ax_response.set_xlim((np.min(t), np.max(t)))

                ax_response.xaxis.set_major_locator(MultipleLocator(1))
                ax_response.xaxis.set_minor_locator(MultipleLocator(0.5))
                ax_response.yaxis.set_major_locator(MultipleLocator(20))
                ax_response.yaxis.set_minor_locator(MultipleLocator(10))
                ax_response.grid(which='major', color='w', linestyle='-', linewidth=1)
                ax_response.grid(which='minor', color='w', linestyle='-', linewidth=0.5)

                plt.legend(loc='upper left')

def plot_response_residuals_stats(data, sc, responses, figsize, om_list=None, std_flag=False, log_flag=False):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """
    # fix colors of detectors

    dt = 0.1048576
    colors = iter(om_cmap(np.linspace(0, 1, len(responses))))
    for om_id in responses:
        c = next(colors)
        if om_id[1] in om_list or om_list is None:

    
            for dataset in range(len(responses[om_id])):

                key = 'response'
                source = responses[om_id][dataset].__repr__()

                fig, (ax_response, ax_error) = plt.subplots(2, 1, sharex=True, figsize=figsize)

                # Error plot
                ax_error.set_title("Relative residuals of response - OM" + str(om_id))
                d = responses[om_id][dataset].mask.adjoint(data[om_id][dataset]).val
                r = sc[source].mean.val[key]
                idx = d > 2
                error = np.abs(d - r)
                sigma = np.sqrt(r)

                t = np.linspace(0, (len(error)-1)*dt, len(error))

                ax_error.plot(t, error/sigma,
                            #label='mean: {0:.2f}'.format(np.nanmean(error[idx]/sigma[idx])), # median {1:.2f}'.format(np.nanmean(error[idx]/sigma[idx]), np.nanmedian(error[idx]/sigma[idx])), 
                            color='k', linewidth=0.5)

                ax_error.hlines(1, xmin=0, xmax=t[-1], colors='r',
                            linestyles='solid', label=r'$\|d - \lambda\|/\sigma = 1$')
                ax_error.hlines(2, xmin=0, xmax=len(error), colors='r',
                            linestyles='dashed', label=r'$\|d - \lambda\|/\sigma = 2$')
                ax_error.set_ylim(0, 5)
                ax_error.set_ylabel(r'Relative residuals in units of $\sigma$')

                # Response plot
                ax_response.set_title("Reconstruction of response - OM" + str(om_id))

                if log_flag:
                    ax_response.plot(t, np.log(d/dt),
                            label='om ' + str(om_id) + r'/data:$d$', color=c, linewidth=0.5)
                    # mean
                    ax_response.plot(t, np.log(r/dt), '.', color='k', label='om ' + str(om_id) + r'/recon:$\lambda$',
                                    markersize=1.0)
                    

                    # error
                    if std_flag:
                        ax_response.fill_between(t, np.log((r-np.sqrt(sc[source].var.val[key]))/0.1),
                                                np.log((r+np.sqrt(sc[source].var.val[key]))/0.1),
                                                color='lightgray', label='std of recon')
                    # ax_response.plot(t, np.log(r - np.sqrt(sc[source].var.val[key])),
                    #         'r+', label='om ' + str(om_id) + '/recon_std')
                    # ax_response.plot(t, np.log(r + np.sqrt(sc[source].var.val[key])),
                    #         'r+')
                    ax_response.set_ylabel(r'ln(lambda)')

                else:
                    ax_response.plot(t, d/dt,
                            label='om ' + str(om_id) + r'/data:$d$', color=c, linewidth=0.5)

                    # mean
                    ax_response.plot(t, r/dt,'.', color='k', label='om ' + str(om_id) + r'/recon:$\lambda$', markersize=1.0)

                    # error
                    # ax_response.plot(t, (r - np.sqrt(sc[source].var.val[key]))/0.1,
                    #         'r+', label='om ' + str(om_id) + '/recon_std')
                    # ax_response.plot(t, (r + np.sqrt(sc[source].var.val[key]))/0.1,
                    #         'r+')
                    if std_flag:
                        ax_response.fill_between(t, (r-np.sqrt(sc[source].var.val[key]))/dt,
                                                (r+np.sqrt(sc[source].var.val[key]))/dt,
                                                color='lightgray', label='std of recon')
                    ax_response.set_ylabel('Photon hit rate in Hz')

                ax_error.set_xlabel('Time steps in s')
                ax_response.legend(loc='upper right')
                ax_error.legend(loc='upper right')
                #ax_error.grid(b=True, which='major', color='#999999', linestyle='-', alpha=0.2)
                ax_error.minorticks_on()
                #ax_error.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
                ax_error.set_xlim((np.min(t), np.max(t)))
                ax_response.legend(loc='upper right')
                #ax_response.grid(b=True, which='major', color='#999999', linestyle='-', alpha=0.2)
                ax_response.minorticks_on()
                #ax_response.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
                ax_response.set_xlim((np.min(t), np.max(t)))

                plt.legend(loc='upper right')


def plot_3d_setup(om_info, with_label=False, with_orientation=False):
    """
    Plotting 3D movement depending on arguments with or without reconstruction.

    :param signal:
    :param oms:
    :param H:
    :param mock_xi:
    :param n_pixels:
    :param title:
    :return:
    """

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_title('Setup 3D')
    om_cmap = plt.get_cmap('nipy_spectral')
    colors = iter(om_cmap(np.linspace(0, 1, len(om_info))))

    if with_orientation:
        if with_label:
            for om, (om_pos, om_dir, _) in om_info.items():
                ax.plot([om_pos[0], om_pos[0] + om_dir[0]],
                        [om_pos[1], om_pos[1] + om_dir[1]],
                        [om_pos[2], om_pos[2] + om_dir[2]],
                        label='om ' + str(om), color=next(colors))

        else:
            tmp_line = 0
            for om, (om_pos, om_dir, _) in om_info.items():
                if om[0] != tmp_line:
                    ax.plot([om_pos[0], om_pos[0] + om_dir[0]],
                            [om_pos[1], om_pos[1] + om_dir[1]],
                            [om_pos[2], om_pos[2] + om_dir[2]],
                            label='line ' + str(om[0]), color=next(colors))
                    tmp_line = om[0]

                else:
                    ax.plot([om_pos[0], om_pos[0] + om_dir[0]],
                            [om_pos[1], om_pos[1] + om_dir[1]],
                            [om_pos[2], om_pos[2] + om_dir[2]],
                            color=next(colors))
                    tmp_line = om[0]
    else:
        tmp_line = 0
        for om, (om_pos, om_dir, _) in om_info.items():
            if om[0] != tmp_line:
                ax.scatter([om_pos[0]],
                           [om_pos[1]],
                           [om_pos[2]],
                           label='line ' + str(om[0]), color=next(colors))
                tmp_line = om[0]

            else:
                ax.scatter([om_pos[0]],
                           [om_pos[1]],
                           [om_pos[2]],
                           color=next(colors))
                tmp_line = om[0]

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.legend()
    plt.show()

def plot_all_lumin_response(sc, oms, sources, ax, figsize, om_cmap=om_cmap, s_cmap=s_cmap):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """

    for axis in ax:

        om_colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
        plt.figure(figsize=figsize)
        plt.title('Movement - x' + axis + ' axis of source: ' + str(sources))

        for om_id in oms:
            i_axis = 1 if axis == 'y' else 2
            plt.plot([oms[om_id].pos[0],
                    oms[om_id].pos[0] + oms[om_id].dir[0]],
                    [oms[om_id].pos[i_axis],
                    oms[om_id].pos[i_axis] + oms[om_id].dir[i_axis]],
                    label='om ' + str(om_id), color=next(om_colors))

        s_colors = iter(s_cmap(np.linspace(0, 1, len(sources))))
        for source in sources:
            if isinstance(source, LS.BioSource):
                n_pixels = source.time_domain.shape[0]
                s_c = next(s_colors)
                source = source.__repr__()
                for _ in range(n_pixels):
                    plt.scatter(sc[source].mean.val['x'][_], sc[source].mean.val[axis][_],
                            marker='o', s=1000*sc[source].mean.val['L'][_]/
                                        np.max(sc[source].mean.val['L']),
                                color=s_c)

                    if _ == 0:
                        plt.scatter(sc[source].mean.val['x'][_], sc[source].mean.val[axis][_],
                                marker='o', s=10,
                                    color=s_c,
                                    label=source)


        plt.legend()
        plt.xlabel('x')
        plt.ylabel(axis)
        plt.axis('equal')

def plot_lumin_response_single(sc, oms, sources, ax, legend, title, figsize, rect_pos_list, rect_size_list, om_cmap=om_cmap, lumin_cmap=s_cmap):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """
    

    for axis in ax:

        plt.figure(figsize=figsize, dpi=160)
        plt.title(title)

        lightcolor = ['white']
        color = ['white']
        i = 0
        for rect_pos, rect_size in zip(rect_pos_list, rect_size_list):

            rect = Rectangle(rect_pos, rect_size[0], rect_size[1], fill=True,alpha=0.3, color=lightcolor[i], zorder=0.0)
            rect1 = Rectangle(rect_pos, rect_size[0], rect_size[1], fill=False, color=color[i], zorder=0.0)
            ax = plt.gca()
            ax.add_patch(rect)
            ax.add_patch(rect1)
            #plt.text(rect_pos[0]+1, rect_pos[1]+rect_size[1]-3,'Volume ' + str(i+1), size=6)
            i = i+1

        
        colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
        line_x = []
        for om_id in oms:

            i_axis = 1 if axis == 'y' else 2
            plt.plot([oms[om_id].pos[0],
                  oms[om_id].pos[0] + oms[om_id].dir[0]],
                 [oms[om_id].pos[i_axis],
                  oms[om_id].pos[i_axis] + oms[om_id].dir[i_axis]],
                 label='om ' + str(om_id), 
                 color=next(colors))
        
            line_x.append(oms[om_id].pos[0])
        if axis == 'z':
            plt.vlines(np.mean(line_x), -100, 100, color='gray', linewidth=0.1)
    
        for source in sources:

           if isinstance(source, LS.BioSource):
                n_pixels = source.time_domain.shape[0]
                source = source.__repr__()

                lumin_colors = iter(lumin_cmap(np.linspace(1, 0.4, n_pixels)))
                for _ in range(n_pixels):
                   lumin_c = next(lumin_colors)
                   if sc[source].mean.val['L'][_] == np.max(sc[source].mean.val['L']):
                    plt.scatter(sc[source].mean.val['x'][_], sc[source].mean.val[axis][_],
                               marker='o', s=20*sc[source].mean.val['L'][_]/np.max(sc[source].mean.val['L'])*float((sc[source].mean.val['L'][_]/np.max(sc[source].mean.val['L']))>0.02),
                               color=lumin_c, label='source')
                   else:
                    plt.scatter(sc[source].mean.val['x'][_], sc[source].mean.val[axis][_],
                               marker='o', s=20*sc[source].mean.val['L'][_]/np.max(sc[source].mean.val['L'])*float((sc[source].mean.val['L'][_]/np.max(sc[source].mean.val['L']))>0.02),
                               color=lumin_c)
                    
        if legend:
            plt.legend()
        plt.xlabel('x in m')
        plt.ylabel(axis + ' in m')
        plt.tight_layout()
        #plt.axis('equal')

def plot_lumin_response(sc, oms, sources_list, label_list, ax, legend, title, figsize, rect_pos_list, rect_size_list, om_cmap=om_cmap, lumin_cmap=s_cmap):
    """
    Plotting signal data with STD (Euler angels, multipole moments).

    :param oms: detector dictionary
    :param H: Hamiltonian or KL
    :param xis: mock xi
    :param mock_data: mock data
    :param title_prefix: title prefix
    :param output_dir: if not given plt is shown
    :return:
    """
    

    for axis in ax:

        plt.figure(figsize=figsize, dpi=160)
        plt.title(title)

        lightcolor = ['lightsteelblue', 'lightgray']
        color = ['slategray', 'gray']
        i = 0
        for rect_pos, rect_size in zip(rect_pos_list, rect_size_list):

            rect = Rectangle(rect_pos, rect_size[0], rect_size[1], fill=True,alpha=0.3, color=lightcolor[i], zorder=0.0)
            rect1 = Rectangle(rect_pos, rect_size[0], rect_size[1], fill=False, color=color[i], zorder=0.0)
            ax = plt.gca()
            ax.add_patch(rect)
            ax.add_patch(rect1)
            plt.text(rect_pos[0]+1, rect_pos[1]+rect_size[1]-3,'Volume ' + str(i+1), size=6)
            i = i+1

        
        colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
        for om_id in oms:

            i_axis = 1 if axis == 'y' else 2
            plt.plot([oms[om_id].pos[0],
                  oms[om_id].pos[0] + oms[om_id].dir[0]],
                 [oms[om_id].pos[i_axis],
                  oms[om_id].pos[i_axis] + oms[om_id].dir[i_axis]],
                 label='om ' + str(om_id), 
                 color=next(colors))
    
        if sources_list is not None:
            sources_color = iter(source_cmap(np.linspace(0,0.9,len(sources_list)))) 
            for i, sl in enumerate(sources_list):
                source_color = next(sources_color)
                for source in sl:

                   if isinstance(source, LS.BioSource):
                        n_pixels = source.time_domain.shape[0]
                        source = source.__repr__()

                        lumin_colors = iter(lumin_cmap(np.linspace(0, 1, n_pixels)))
                        for _ in [15]:
                           lumin_c = next(lumin_colors)
                           plt.scatter(sc[i][source].mean.val['x'][_], sc[i][source].mean.val[axis][_],
                                   marker='o', s=20,
                                   color=source_color, alpha=1.0, 
                                   label=label_list[i])
#s=100*sc[i][source].mean.val['L'][_]/
#  np.max(sc[i][source].mean.val['L'])
        if legend:
            plt.legend( bbox_to_anchor=(1.06, 1.05), loc='upper left')
        plt.xlabel('x in m')
        plt.ylabel(axis + ' in m')
        plt.tight_layout()
        #plt.axis('equal')

def plot_movement_3d(sc, sources, oms, KL, title, error_bars=False):
    """
    Plotting 3D movement depending on arguments with or without reconstruction.

    :param signal:
    :param oms:
    :param KL:
    :param xi:
    :param n_pixels:
    :param title:
    :return:
    """
    om_cmap = plt.get_cmap('nipy_spectral')

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ax = fig.add_subplot(111, projection='3d')
    ax.set_title(title)

    for source in sources:
        if isinstance(source, LS.BioSource):
            n_pixels = source.time_domain.shape[0]
            source = source.__repr__()

            lumin_cmap = plt.get_cmap('Greens')
            lumin_colors = iter(lumin_cmap(np.linspace(0, 1, n_pixels)))
            for _ in range(n_pixels):
                if KL is not None:
                    ax.scatter(sc[source].mean.val['x'][_],
                               sc[source].mean.val['y'][_],
                               sc[source].mean.val['z'][_],
                               marker='o',
                               s=1000 * sc[source].mean.val['L'][_] /
                                 np.max(sc[source].mean.val['L']),
                               alpha=0.5, color=next(lumin_colors))

                    # error

                    if error_bars:

                        ax.plot([sc[source].mean.val['x'][_] - np.sqrt(sc[source].var.val['x'][_]),
                                 sc[source].mean.val['x'][_] + np.sqrt(sc[source].var.val['x'][_])],
                                [sc[source].mean.val['y'][_], sc[source].mean.val['y'][_]],
                                [sc[source].mean.val['z'][_], sc[source].mean.val['z'][_]],
                                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

                        ax.plot([sc[source].mean.val['x'][_], sc[source].mean.val['x'][_]],
                                [sc[source].mean.val['y'][_] - np.sqrt(sc[source].var.val['y'][_]),
                                 sc[source].mean.val['y'][_] + np.sqrt(sc[source].var.val['y'][_])],
                                [sc[source].mean.val['z'][_], sc[source].mean.val['z'][_]],
                                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

                        ax.plot([sc[source].mean.val['x'][_], sc[source].mean.val['x'][_]],
                                [sc[source].mean.val['y'][_], sc[source].mean.val['y'][_]],
                                [sc[source].mean.val['z'][_] - np.sqrt(sc[source].var.val['z'][_]),
                                 sc[source].mean.val['z'][_] + np.sqrt(sc[source].var.val['z'][_])],
                                linestyle='dashed', alpha=n_pixels / (n_pixels + 2 * _), c='r')

            if oms is not None:
                colors = iter(om_cmap(np.linspace(0, 1, len(oms))))
                for om_id in oms:
                    ax.plot([oms[om_id].pos[0], oms[om_id].pos[0] + oms[om_id].dir[0]],
                            [oms[om_id].pos[1], oms[om_id].pos[1] + oms[om_id].dir[1]],
                            [oms[om_id].pos[2], oms[om_id].pos[2] + oms[om_id].dir[2]],
                            label='om ' + str(om_id),
                            color=next(colors))
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')
            plt.legend()
            plt.show()

def plot_3d_setup(om_info, with_label=False, with_orientation=False):
    """
    Plotting 3D movement depending on arguments with or without reconstruction.

    :param signal:
    :param oms:
    :param H:
    :param mock_xi:
    :param n_pixels:
    :param title:
    :return:
    """

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_title('Setup 3D')

    colors = iter(om_cmap(np.linspace(0, 1, len(om_info))))

    if with_orientation:
        if with_label:
            for om, pos, dir in om_info:
                ax.plot([pos[0], pos[0] + dir[0]],
                        [pos[1], pos[1] + dir[1]],
                        [pos[2], pos[2] + dir[2]],
                        label='om ' + str(om), color=next(colors))

        else:
            for om, pos, dir in om_info:
                if om[1] == 1:
                    ax.plot([pos[0], pos[0] + dir[0]],
                            [pos[1], pos[1] + dir[1]],
                            [pos[2], pos[2] + dir[2]],
                            label='line ' + str(om[0]), color=next(colors))

                else:
                    ax.plot([pos[0], pos[0] + dir[0]],
                            [pos[1], pos[1] + dir[1]],
                            [pos[2], pos[2] + dir[2]],
                            color=next(colors))
    else:
        for om, pos, dir in om_info:
            if om[1]==1:
                ax.scatter([pos[0]],
                        [pos[1]],
                        [pos[2]],
                        label='line ' + str(om[0]), color=next(colors))


            else:
                ax.scatter([pos[0]],
                        [pos[1]],
                        [pos[2]],
                        color=next(colors))

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.legend()
    plt.show()



