# LuminTracing

We developed a generative model of the measurement process of multiple optical modules (OMs) and the light signal generation of biological sources (light sources) in order to track luminescent source in the deep sea.
The model parameters such as source movement and characteristic light curves are reconstructed by a Variational Bayesian Inference algorithm, called Metric Gaussian Variational Inference ([MGVI](https://arxiv.org/abs/1901.11033)).  
The [NIFTy](https://ift.pages.mpcdf.de/nifty/index.html) (Numerical Information Field Theory) framework provides an implementation of the MGVI algorithm.

OMs are typically used in neutrino telescopes, such as the ANTARES telescope which is located on the bottom of the Mediterranean Sea. It is recording the light activity for more than ten years resulting in a big dataset, full of light emissions of organisms in the deep sea. Our model was tested on data of the ANTARES telescope, but can also applied to various telescopes consisting of optical modules.

A precise description of our model, as well as the test results can be found in ??.

**Currently, no data can be made publically available. If you are interested to test the model, please contact me directly.**




## Requirements

- Nifty (v6)
- Numpy
- mpi4py (optional) 
- argparse
- importlib
- shutil

## Dataset
![alt text](docs/img/example_data.jpg)

**Currently no sample data is available**

Since no sample data is available the scripts can not be executed.
All scripts rely on a configuration file of the detector.
We are working on a synthetic telescope setup with synthetic data.

## Generative model

A precise description of our model, as well as the test results can be found in ??.
![alt text](docs/img/response.jpg)

## Inference

A detailed description of the inference will follow, once synthetic data is available. 

**Please contact me directly, if you are interested to run the script.**

