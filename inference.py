import sys
sys.path.append('..')
import nifty6 as ift
import importlib
import argparse
from shutil import copyfile
from ANTARES.utils import helpers
from ANTARES.utils import inference as infer_utils
from ANTARES.utils import preprocess
from ANTARES.utils import plot
from ANTARES.models import LightSources as LS
import time
from mpi4py import MPI
import numpy as np

###################################################################################################
# Args Parser for Inference
###################################################################################################
# ONLY: input settings

description= 'Start inference ' \
             'by passing seed, config file and output directory. ' \
             'Optional flags can be set to adjust inference settings.'

parser = argparse.ArgumentParser(description=description)

# Parser arguments

parser.add_argument('-s', '--seed', dest='seed',
                    type=int, required=True, help='seed')
parser.add_argument('-n', '--n', dest='n',
                    type=int, required=False, help='n=0, H_init=0')
parser.add_argument('-c', '--config', dest='config',
                    required=True,
                    help='path to config file')
parser.add_argument('-o', '--output', dest='output',
                    required=True,
                    help='path to output dir')
parser.add_argument('-i', '--input', dest='input',
                    default=None,
                    help='path to input dir')

# Parser flags
parser.add_argument('--data', dest='data',
                    default=False, action='store_true',
                    help='to reload data from input directory. '
                         'Default: data from config file.')
parser.add_argument('--oms', dest='oms',
                    default=False, action='store_true',
                    help='to reload OMs from input directory. '
                         ' Default: OMs from config file.')
parser.add_argument('--sources', dest='sources',
                    default=False, action='store_true',
                    help='to reload sources from input directory. '
                         'Default: sources from config file.')
parser.add_argument('--reload', dest='reload',
                    default=False, action='store_true',
                    help='to reload data, OMs, sources from input directory. '
                         'Default: all from config file.')

parser.add_argument('--KL', dest='KL',
                    default=False, action='store_true',
                    help='to load KL. Default: create random')
parser.add_argument('--KL_LUMIN_POS', dest='KL_LUMIN_POS',
                    default=False, action='store_true',
                    help='to load KL lumin pos. Default: create random')
parser.add_argument('--KL_LUMIN_POS_OMS', dest='KL_LUMIN_POS_OMS',
                    default=False, action='store_true',
                    help='to load KL lumin pos. Default: create random')
parser.add_argument('--KL_POS', dest='KL_POS',
                    default=False, action='store_true',
                    help='to load KL pos. Default: create random')
parser.add_argument('--KL_LUMIN', dest='KL_LUMIN',
                    default=False, action='store_true',
                    help='to load KL lumin. Default: create random')

parser.add_argument('--minimize', dest='minimize',
                    default=False, action='store_true',
                    help='to turn minimization on.')
parser.add_argument('--stats', dest='stats',
                    default=False, action='store_true',
                    help='to turn stats calc on.')
parser.add_argument('--prior', dest='prior',
                    default=False, action='store_true',
                    help='to turn prior plotting on.')


parser.add_argument('--p', dest='parallel',
                    default=False, action='store_true',
                    help='to turn parallelizing on')
parser.add_argument('--f', dest='force',
                    default=False, action='store_true',
                    help='to force running')

args = parser.parse_args()
#args = parser.parse_args(['-c', '/home/nico/Master/BioLuminescence/code/test/config',
#                          '-s', '0',
#                          '-o', '/home/nico/Master/BioLuminescence/out/285702775_L08OM45_100_1_final_fixed_seed0_0',
#                          '-n', 0,
#                          '--minimize',
#                          '--stats'])

# Import config file
cfg = helpers.import_config(args.config)

# Reload all flag
if args.reload:
    args.sources = True
    args.oms = True
    args.KL = True
    args.data = True

# Parallel flag
if args.parallel:
    comm = MPI.COMM_WORLD
    ntask = comm.Get_size()
    rank = comm.Get_rank()
    master = (rank == 0)
    mpi = ntask > 1
else:
    comm = None
    rank = 0
    ntask = 1
    master = (rank == 0)

if master:
    print('Paths:')
    print('\tConfig path:' + str(args.config))
    print('\tOutput path:' + str(args.output))
    print('\tSeed:' + str(args.seed))
    print('\tInit sampling:' + str(args.n))
    if args.input is not None:
        print('\tInput path:' + str(args.input))

    print('\nFlags:')
    print('\tReload flag:' + str(args.reload))
    print('\t\tSource flag:' + str(args.sources))
    print('\t\tOMS flag:' + str(args.oms))
    print('\t\tData flag:' + str(args.data))

    print('\n\tminimize:' + str(args.minimize))
    print('\n\tstats:' + str(args.stats))
    print('\n\tprior:' + str(args.prior))

    print('\n\tKL flag:' + str(args.KL))
    print('\tKL lumin pos flag:' + str(args.KL_LUMIN_POS))
    print('\tKL lumin pos oms flag:' + str(args.KL_LUMIN_POS_OMS))
    print('\tKL lumin flag:' + str(args.KL_LUMIN))
    print('\tKL pos flag:' + str(args.KL_POS))

    print('\n\tParallel flag:' + str(args.parallel))
    if args.parallel is not None:
        print('\t\tWorld size:' + str(ntask))
        print('\t\tRank:' + str(rank))



if master and not args.force:
    user_command = input("Press ENTER to run or any key to abort: ")
else:
    user_command = ''

if args.parallel:
    user_command = comm.bcast(user_command, root=0)
    if user_command == '':
        print('Rank ' + str(rank) + ' is running.')
    else:
        print('Rank ' + str(rank) + ' closed.')
        exit()

if master:
    print('Inference is running.')

###############################################################################
#%% Standard variables
###############################################################################

# Set variables
SEED = args.seed
OUTPUT_DIR = args.output
SOURCE_FILE = '/sources.pickle'
OM_FILE = '/om.pickle'
RESPONSES_FILE = '/responses.pickle'
STATS_FILE = '/stats.pickle'
H_FILE = '/H.pickle'
DATA_OUTPUT_FILE = '/data.pickle'
KL_FILE = '/KL.pickle'

ift.random.push_sseq_from_seed(SEED)

# Create output directory
if master:
    helpers.create_dir(OUTPUT_DIR)

    # Save args
    keys = ['args']
    helpers.save_globals(OUTPUT_DIR, globals(), keys)
    copyfile(args.config, args.output + '/config.py')

###############################################################################
#%% Reload or create
###############################################################################
# IMPORTANT: if you want to understand how classes are initilized

if master:
    if args.input is not None:
        print('Reload data...')
        INPUT_DIR = args.input

    if args.data and args.input is not None:
        data, responses, om_info, domains = helpers.load(INPUT_DIR + DATA_OUTPUT_FILE)
    else:
        # IMPORTANT: constructs response, needs data to set domain
        data, responses, om_info, domains, deleted_oms = preprocess.create_data_response(cfg.DATA_FILE,
                                                                            cfg.EFF_FILE,
                                                                            cfg.OM_ID,
                                                                            cfg.DATA_SAMPLE,
                                                                            cfg.CUSTOM_MASK,
                                                                            cfg.MIRROR_DATA)
    if args.oms and args.input is not None:
        oms = helpers.load(INPUT_DIR + OM_FILE)
    else:
        oms = cfg.create_oms(om_info)
    
    if args.sources and args.input is not None:
        sources, source_dict = helpers.load(INPUT_DIR + SOURCE_FILE)
    else:
        # IMPORTANT: see config file
        sources, source_dict = cfg.create_sources(domains, om_info)


    if not args.sources or not args.oms or not args.data:
        # IMPORTANT: actually applies NIFTy operator on sources
        _ = preprocess.call_responses(oms, source_dict, responses, om_info, output_dir=None)


    # mock_xi, data, ground_truth =
    # infer_utils.create_mock_data(sources, oms, output_dir=OUTPUT_DIR)

###############################################################################
# %% Saving and plotting
###############################################################################

    # saving
    helpers.save(oms, OM_FILE, OUTPUT_DIR)
    helpers.save([data, responses, om_info, domains], DATA_OUTPUT_FILE, OUTPUT_DIR)
    helpers.save([sources, source_dict], SOURCE_FILE, OUTPUT_DIR)

    # Plot setup and data
    for i in range(len(cfg.DATA_SAMPLE)):
        plot.plot_data(responses, xis=None, data=data, dataset=i,
                       title_prefix='Data' + str(i),
                       output_dir=OUTPUT_DIR + '/data')

else:
    data = None
    responses = None
    domains = None
    om_info = None
    oms = None
    sources = None
    source_dict = None


if args.parallel:
    data, responses, om_info, domains = comm.bcast([data, responses, om_info, domains], root=0)
    oms = comm.bcast(oms, root=0)
    sources, source_dict = comm.bcast([sources, source_dict], root=0)

###############################################################################
#%% Setup minimization
###############################################################################

if master:
    # IMPORTANT: NIFTy objects, see NIFTy doc
    ic_sampling = ift.AbsDeltaEnergyController(
        deltaE=cfg.SAMPLING_DELTA_E,
        iteration_limit=cfg.SAMPLING_ITERATION_LIMIT)
    ic_newton = ift.AbsDeltaEnergyController(
        name='Newton',
        deltaE=cfg.MINIMIZATION_DELTA_E,
        iteration_limit=cfg.MINIMIZATION_ITERATION_LIMIT)

    minimizer = ift.NewtonCG(ic_newton)

    # Initialize minimization
    # IMPORTANT: but easy, builds Poisson likelihood from response and data (no
    # specific noise)
    H, likelihood,  H_time = infer_utils.get_H(responses, data, ic_sampling, output_dir=None)
    H_rand = ift.MultiField.from_random(H.domain, 'normal')
    H_0 = ift.MultiField.full(H.domain, 0.0)

    H_init_save = None
    energy_save = None

    # IMPORTANT: only for reload flag, how to reuse excitations
    if args.KL:

        sc = helpers.load(INPUT_DIR + STATS_FILE)
        KL = sc['KL'].mean
        H_init = KL

    if args.KL_LUMIN_POS:

        sc = helpers.load(INPUT_DIR + STATS_FILE)
        KL = sc['KL'].mean
        H_init = [H_0]
        #H_init.append(KL)
        for source in sources:
            if isinstance(source, LS.BioSource):
                H_init.append(KL.extract(source.pos0.domain))
            H_init.append(KL.extract(source.lumin.domain))
            #H_init.append(ift.MultiField.full(source.pos.domain, 0))
        H_init = ift.MultiField.union(H_init)

        energy_save = H(H_init).val
        H_init_save = H_init

    if args.KL_LUMIN_POS_OMS:

        sc = helpers.load(INPUT_DIR + STATS_FILE)
        KL = sc['KL'].mean
        H_init = [H_0]
        #H_init.append(KL)
        for source in sources:
            if isinstance(source, LS.BioSource):
                H_init.append(KL.extract(source.pos0.domain))
            H_init.append(KL.extract(source.lumin.domain))
            #H_init.append(ift.MultiField.full(source.pos.domain, 0))
        for om in oms:
            H_init.append(KL.extract(oms[om].eff.domain))
            H_init.append(KL.extract(oms[om].eff_std.domain))
        H_init = ift.MultiField.union(H_init)

        energy_save = H(H_init).val
        H_init_save = H_init

    if args.KL_LUMIN:

        sc = helpers.load(INPUT_DIR + STATS_FILE)
        KL = sc['KL'].mean
        H_init = [H_rand]
        #H_init.append(KL)
        for source in sources:
            if isinstance(source, LS.BioSource):
                H_init.append(KL.extract(source.lumin.domain))
                #H_init.append(ift.MultiField.full(source.pos.domain, 0))
        H_init = ift.MultiField.union(H_init)

        energy_save = H(H_init).val
        H_init_save = H_init

    
    if args.KL_POS:

        sc = helpers.load(INPUT_DIR + STATS_FILE)
        KL = sc['KL'].mean
        H_init = [H_rand]
        #H_init.append(KL)
        for source in sources:
            if isinstance(source, LS.BioSource):
                H_init.append(KL.extract(source.pos0.domain))
        H_init = ift.MultiField.union(H_init)

        energy_save = H(H_init).val
        H_init_save = H_init

else:
    ic_sampling = None
    ic_newton = None
    minimizer = None
    KL = None
    H = None
    H_init = None
    H_init_save = None
    energy_save = None

if args.parallel:
    ic_sampling = comm.bcast(ic_sampling, root=0)
    ic_newton = comm.bcast(ic_newton, root=0)
    minimizer = comm.bcast(minimizer, root=0)
    H = comm.bcast(H, root=0)
    H_init_save = comm.bcast(H_init_save, root=0)
    energy_save = comm.bcast(energy_save, root=0)

if not args.KL and not args.KL_LUMIN_POS and not args.KL_LUMIN_POS_OMS:

    # IMPORTANT: n flag: guessing good initial excitations
    n = args.n

    if n == 0:
        H_init = []
        H_init.append(ift.MultiField.full(H.domain, 0.))
        for source in sources:
            if isinstance(source, LS.BioSource):
                H_init.append(ift.MultiField.from_random(source.lumin.domain))
                #H_init.append(ift.MultiField.from_random(source.pos.domain))
        H_init = ift.MultiField.union(H_init)
        H_init_save = H_init
        energy_save = H(H_init_save).val
    if n == 1:
        H_init = 4 * ift.MultiField.from_random(H.domain, 'normal')
    else:
        

        for i in range(8):
       
#            for _ in range(n):
#                for source in sources:
#                    if isinstance(source, LS.BioSource):
#                        H_init = ift.MultiField.unite(H_init, ift.MultiField.from_random(source.lumin.domain))
#
#                energy = H(H_init).val
#                if energy_save is None:
#                    energy_save = energy
#                    H_init_save = H_init
#                else:
#                    if energy < energy_save:
#                        energy_save = energy
#                        H_init_save = H_init
#
            for _ in range(n):
                for source in sources:
                    if isinstance(source, LS.BioSource):
                        H_init = ift.MultiField.unite(H_init_save, ift.MultiField.from_random(source.pos.domain))

                energy = H(H_init).val
                if energy_save is None:
                    energy_save = energy
                    H_init_save = H_init
                else:
                    if energy < energy_save:
                        energy_save = energy
                        H_init_save = H_init

        if args.parallel:
            energy_save = comm.gather(energy_save,  root=0)
            H_init_save = comm.gather(H_init_save, root=0)

        if master:

            print('Minimizing H init.')

            if isinstance(energy_save, list):
                idx = np.argmin(energy_save)
                print(energy_save)
                H_init = H_init_save[idx]
            else:
                H_init = H_init_save

if args.parallel:
    H_init = comm.bcast(H_init, root=0)

###############################################################################
# %% Dimensionality
###############################################################################
if master:

    dim = 0
    for key in H_init.val.keys():
        dim += H_init.val[key].size
    print('Dimensionality of H: {0}'.format(dim))

###############################################################################
# %% Saving priors
###############################################################################

if master and args.prior:

    H_prior_list = []
    for p in range(12):
        H_prior_save = ift.MultiField.from_random(H.domain, 'normal')
        energy_save = H(H_prior_save).val
        for _ in range(1):
            H_prior = ift.MultiField.from_random(H.domain, 'normal')
            energy = H(H_prior).val
            if energy < energy_save:
                energy_save = energy
                H_prior_save = H_prior

        H_prior_list.append((H_prior_save, '/prior' + str(p), None))

    sc, SC_time = infer_utils.get_stat_calcs(minimized_obj=[H_prior, H, 100],
                                            sources=None,
                                            oms=None,
                                            responses=responses,
                                            output_dir=OUTPUT_DIR)

        # Plot prior
    for i in range(len(cfg.DATA_SAMPLE)):
        plot.plot_data({(4,44):responses[(4,44)]}, xis=H_prior_list, data=data, dataset=i,
                    title_prefix='logprior' + str(i),
                    output_dir=OUTPUT_DIR + '/prior', log_flag=True)

        plot.plot_data({(4,44):responses[(4,44)]}, xis=H_prior_list, data=data, dataset=i,
                    title_prefix='prior' + str(i),
                    output_dir=OUTPUT_DIR + '/prior')

        #plot.plot_signal(sources, oms, responses, xis=[(H_prior, '/prior', 'ko')],
        #                title_prefix="prior" + str(p),
        #                output_dir=OUTPUT_DIR + '/prior')
        #plot.plot_movement_2d(sources, oms, xis=[(H_prior, '/prior', 'ko')],
        #                    title_prefix="prior" + str(p), one_plot=True,
        #                    output_dir=OUTPUT_DIR + '/prior')
###############################################################################
# %% Saving and plotting
###############################################################################

if master:

    # Save init
    helpers.save([H_init, H], H_FILE, OUTPUT_DIR)

    # Plot init
    for i in range(len(cfg.DATA_SAMPLE)):
        plot.plot_data(responses, xis=[(H_init, '/init', None)], data=data, dataset=i,
                       title_prefix='Init' + str(i),
                       output_dir=OUTPUT_DIR + '/init')

        plot.plot_data(responses, xis=[(H_init, '/init', None)], data=data, dataset=i,
                       log_flag=True,
                       title_prefix='Init' + str(i),
                       output_dir=OUTPUT_DIR + '/init_log')

    plot.plot_signal(sources, oms, responses, xis=[(H_init, '/init', 'ko')],
                     title_prefix="Init",
                     output_dir=OUTPUT_DIR + '/init')
    plot.plot_movement_2d(sources, oms, xis=[(H_init, '/init', 'ko')],
                          title_prefix='Init', one_plot=True,
                          output_dir=OUTPUT_DIR + '/init')


###############################################################################
#%% Minimize
###############################################################################
# IMPORTANT: MGVI is behind this: ANTARES/utils/inference.py minimize_KL
if args.minimize:
    KL, summary, KL_time = infer_utils.minimize_KL(sources, oms, responses, data, domains,
                               H, H_init=H_init,
                               minimizer=minimizer, constants=[],
                               n_samples=cfg.N_SAMPLES,
                                   sample_increase=cfg.SAMPLE_INCREASE,
                                   sample_mod=cfg.SAMPLE_MOD,
                               n_global_iterations=cfg.N_GLOBAL_ITERATIONS,
                               iteration_limit=cfg.MINIMIZATION_ITERATION_LIMIT,
                               n_local_iterations=cfg.N_LOCAL_ITERATIONS,
                                   local_mod=cfg.LOCAL_MOD,
                                   local_increase=cfg.LOCAL_INCREASE,
                               comm=comm,
                               seed=SEED, output_dir=OUTPUT_DIR, plot_flag=cfg.PLOT_FLAG)

###############################################################################
#%% Saving
###############################################################################

if master and args.minimize:
    helpers.save(KL.position, KL_FILE, OUTPUT_DIR)

###############################################################################
#%% Plot reconstruction
###############################################################################

if master:
    if args.minimize:
        for i in range(len(cfg.DATA_SAMPLE)):
            plot.plot_data(responses, xis=[(KL.position, '/recon', None)], data=data, dataset=i,
                        title_prefix='Recon' + str(i),
                        output_dir=OUTPUT_DIR + '/data')

            plot.plot_data(responses, xis=[(KL.position, '/recon', None)], data=data, dataset=i,
                        log_flag=True,
                        title_prefix='Recon' + str(i),
                        output_dir=OUTPUT_DIR + '/data_log')

        plot.plot_signal(sources, oms, responses, xis=[(KL.position, '/recon', 'ro')],
                        title_prefix="Recon",
                        output_dir=OUTPUT_DIR + '/signal')
        plot.plot_movement_2d(sources, oms, xis=[(KL.position, '/recon', 'ro')],
                            title_prefix='Recon',
                            output_dir=OUTPUT_DIR + '/movement')
    else:

        for i in range(len(cfg.DATA_SAMPLE)):
            plot.plot_data(responses, xis=[(H_init, '/recon', None)], data=data, dataset=i,
                        title_prefix='Recon' + str(i),
                        output_dir=OUTPUT_DIR + '/data')

            plot.plot_data(responses, xis=[(H_init, '/recon', None)], data=data, dataset=i,
                        log_flag=True,
                        title_prefix='Recon' + str(i),
                        output_dir=OUTPUT_DIR + '/data_log')

        plot.plot_signal(sources, oms, responses, xis=[(H_init, '/recon', 'ro')],
                        title_prefix="Recon",
                        output_dir=OUTPUT_DIR + '/signal')
        plot.plot_movement_2d(sources, oms, xis=[(H_init, '/recon', 'ro')],
                            title_prefix='Recon',
                            output_dir=OUTPUT_DIR + '/movement')
###############################################################################
#%% Stats
###############################################################################

if master and args.stats:
    # IMPORTANT: just to get to know StatCalculator for posterior and error
    # estimates
    if args.minimize:
        sc, SC_time = infer_utils.get_stat_calcs(minimized_obj=[KL.position, H, cfg.RECON_N_SAMPLES],
                                                sources=sources,
                                                oms=oms,
                                                responses=responses,
                                                output_dir=OUTPUT_DIR)
    else: 
        sc, SC_time = infer_utils.get_stat_calcs(minimized_obj=[H_init, H, cfg.RECON_N_SAMPLES],
                                                sources=sources,
                                                oms=oms,
                                                responses=responses,
                                                output_dir=OUTPUT_DIR)


    helpers.save(sc, STATS_FILE, OUTPUT_DIR)

    plot.plot_movement_2d_error(sc, oms, sources,
                                title_prefix='_error',
                                output_dir=OUTPUT_DIR + '/movement_error')
    plot.plot_signal_stats(sc, xis=None, oms=oms,
                           title_prefix='_error',
                           output_dir=OUTPUT_DIR + '/signal_error')
    plot.plot_response_stats(data, sc, responses, sources,
                             title_prefix='error',
                             output_dir=OUTPUT_DIR + '/response_error')
    plot.plot_response_stats(data, sc, responses, sources,
                             log_flag=True,
                             title_prefix='error',
                             output_dir=OUTPUT_DIR + '/response_error_log')
    plot.plot_lumin_response(sc, oms, sources,
                             title_prefix='lumin',
                             output_dir=OUTPUT_DIR + '/movement_error')

    print("get_H \tComputation time: " + str(H_time * 1000000) + str(" s"))
    if args.minimize:
        print("minimize_KL \tComputation time: " + str(KL_time * 1000000) + str(" s"))
        print("get_stat_cals \tComputation time: " + str(SC_time * 1000000) + str(" s"))
        print("Global itertations: " + str(summary[0]))
        print("Local itertations: " + str(summary[1]))
        print("Local itertation limits: " + str(cfg.MINIMIZATION_ITERATION_LIMIT))
        print("N samples: " + str(summary[2]))
    print("H = " + str(sc['H'].mean.val))
    print("Ouput directory: " + str(OUTPUT_DIR))
