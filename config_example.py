from ANTARES.utils import helpers
from ANTARES.models import LightSources as LS
from ANTARES.models.OMS import OM
import time
import nifty6 as ift
import numpy as np
import ANTARES.utils.nifty as nifty_utils

###############################################################################
### Initialize variables ######################################################
###############################################################################

# INPUT #######################################################################
#
# set data vector size and location of ANTARES data file
# analyze ANTARES data file with tests/show_data.py
#
# set type of data
# see in ANTARES/utils/preprocess.py create_mask()
# for more types
#
# [(starting_index, final_index, type)]
DATA_SAMPLE = [(350, 450, 'signal')]
LINES = [4]
OMS = [34,51]

# DEFINE POSITION MODEL #######################################################
# see ANTARES/model/LightSource.py Uni/GaussBioSource
#
# Gaussian position model
# (x,y,z)
INIT_POSITION = [40, 0, 20] #45,0,20
# [std of mean, std, std of std]
INIT_GAUSS = [1, 5, 1] #{}
# put all together and provide this as position0
GAUSS_POSITION = (INIT_POSITION,INIT_GAUSS)

# Uniform position model
# [(x_start, x_end), (y_start, y_end), (z_start, z_end)]
# provide this as position 0
UNI_POSITION = [(20, 60), (-20, 20), (0, 40)]

# SOME DATA PREPROCESSING #####################################################
# see ANTARES/utils/preprocess.py create_data_response
# and tests/inference.py
MIRROR_DATA = False

# PLOTTING SETTINGS ###########################################################
# see ANTARES/utils/inference.py minimize_KL
# and tests/inference.py
PLOT_FLAG = False 

# MINIMIZATION ################################################################
# see ANTARES/utils/inference.py minimize_KL
# and tests/inference.py
#
# number of samples drawn by MGVI per global iteration
N_SAMPLES = 0
# adding number of samples after SAMPLE_MOD global iterations
SAMPLE_INCREASE = 2
# when to add SAMPLE_INCREASE
SAMPLE_MOD = 4

# number of global iterations / number of mgvi samplings
N_GLOBAL_ITERATIONS = 16

# number of minimization steps within one global iteration
N_LOCAL_ITERATIONS = 0
# adding number of minimization steps after SAMPLE_MOD global iterations
LOCAL_INCREASE = 2
# when to add LOCAL_INCREASE
LOCAL_MOD = 4

# hard stop for minimization
# MEANING: 4 local iterations and 3 as iteration limit
#           results in 12 iteration steps
MINIMIZATION_ITERATION_LIMIT = 8

# minimizer settings
MINIMIZATION_DELTA_E = 1e-2
SAMPLING_ITERATION_LIMIT = 100
SAMPLING_DELTA_E = 1e-16
RECON_N_SAMPLES = 16

# SYSTEM INFORMATION ##########################################################
# get current system (server/local/...)
# see ANTARES/utils/helpers.py get_base
# if not implemented there it needs to be hardcoded
BASE_DIR = helpers.get_base()
# TODO: Make data available
#DATA_FILE = BASE_DIR + "grb/2010/Antares_110_284875971_ALL.csv"
# TODO: Make data available
#EFF_FILE = BASE_DIR + "eff_tables/Jan3_2010.txt"

###############################################################################
### Lines and OMs #############################################################
###############################################################################
# no changes needed
# function called in tests/inference to get list of OMS
# missing oms are stated in last section: source_dict
# (line, om)
OM_ID = []
for l in LINES:
    for om in range(OMS[0],OMS[1] + 1):
        OM_ID.append((l, om))



###############################################################################
###  Create data and oms ######################################################
###############################################################################
# define efficiency model and attenuation length model
# as always (mean, std of mean, std, std of std)
# called in tests/inference.py
def create_oms(om_info):

    oms = {}

    for om_id in om_info:

        eff=om_info[om_id][2]/1.1 ### changable
        om = OM(om_pos=om_info[om_id][0],
                om_dir=om_info[om_id][1],
                om_id=om_id,
                eff_init=[eff,0.001,0.001,0.0001], ### changable
                l_att_init=[60,0.0,0.5001,0.0]) ### changeable

        oms[om_id] = om

    return oms

###############################################################################
### Create sources ############################################################
###############################################################################

### CUSTOM SOURCE #############################################################
# see ANTARES/models/LightSource.py
#
# ActLuminSource does not include position model!!!
# UniBioSource includes position model but no lumin model!!!
#
# inherit both -> including lumin and position model

class CustomSource(LS.ActLuminBioSource, LS.UniBioSource):

    def __init__(self, domain, padding=1.5, source_id=0,
                position0=None,
                dct_velocity=None,
                dct_ps=None,
                t0_range=[[0,1]]):

        start_time = time.time()
        super().__init__(domain=domain, 
                            padding=padding, 
                            source_id=source_id, 
                            position0=position0,
                            dct_velocity=dct_velocity,
                            dct_ps=dct_ps, 
                            t0_range=t0_range)
        

        self.init_time = time.time() - start_time
        print("Initialize:\tCustomSource\tComputation time: " +
            str(self.init_time * 1000) + str(" ms"))

### SOURCE AND OMS RELATIONS ##################################################

def create_sources(domains):

    # padding factor (no changes needed)
    padding_factor = 1.5

    # Create sources constant since fluactuation etc. are 1e-8
    # only mean and std and std of std are set

    dct_lumin = [
        {
            'offset_mean': 5000,
            'offset_std_mean': 500,
            'offset_std_std': 0.1},
        {
            'fluctuations_mean': 1e-8,
            'fluctuations_stddev': 1e-8,
            'flexibility_mean': 1e-8,
            'flexibility_stddev': 1e-8,
            'asperity_mean': 1e-8,
            'asperity_stddev': 1e-8,
            'loglogavgslope_mean': -4,
            'loglogavgslope_stddev': 1e-1, }
    ]

    # Define noise sources depending on number of storeys
    # IMPORTANT: change source id accordingly

    noise_source0 = LS.ConstNoiseSource(domains[0], padding_factor, source_id=str(0),
                                        dct_lumin=dct_lumin)
    noise_source1 = LS.ConstNoiseSource(domains[0], padding_factor, source_id=str(1),
                                        dct_lumin=dct_lumin)
    noise_source2 = LS.ConstNoiseSource(domains[0], padding_factor, source_id=str(2),
                                        dct_lumin=dct_lumin)
    noise_source3 = LS.ConstNoiseSource(domains[0], padding_factor, source_id=str(3),
                                        dct_lumin=dct_lumin)
    noise_source4 = LS.ConstNoiseSource(domains[0], padding_factor, source_id=str(4),
                                        dct_lumin=dct_lumin)
    noise_source5 = LS.ConstNoiseSource(domains[0], padding_factor, source_id=str(5),
                                        dct_lumin=dct_lumin)

    # Bio Burst Source

    # in case of movement

    dct_velocity = [
        {
            'offset_mean': 0.1,
            'offset_std_mean': 0.1,
            'offset_std_std': 0.1},
        {
            'fluctuations_mean': 0.05,
            'fluctuations_stddev': 0.01,
            'flexibility_mean': 0.001,
            'flexibility_stddev': 0.001,
            'asperity_mean': 0.001,
            'asperity_stddev': 0.001,
            'loglogavgslope_mean': -4,
            'loglogavgslope_stddev': 1e-3}
    ]

    dct_ps = {'plaw':1.5, # power law
              'amp': 8, # amplitude
              'zeromode': 2, # zeromode or standard deviation
              'offset': 4} # constant offset (log model)

    # define bio source
    bio_source = CustomSource(domains[0], padding_factor, source_id='bio',
                              position0=UNI_POSITION, # see top of file
                              dct_ps=dct_ps, # power spectrum
                              dct_velocity=None, # change in case of movement
                              t0_range=[[1/3,2/3]]) # position of t0 for act fct

    # RELATION BETWEEN OMS AND SOURCES
    # OMS(LINES[0], n) sees noise_source and bio_source
    source_dict = {(LINES[0], 34): [[noise_source5, bio_source]],
                   (LINES[0], 35): [[noise_source5, bio_source]],
                   (LINES[0], 36): [[noise_source5, bio_source]],
                   (LINES[0], 37): [[noise_source0, bio_source]],
                   (LINES[0], 38): [[noise_source0, bio_source]],
                   (LINES[0], 39): [[noise_source0, bio_source]],
                   (LINES[0], 40): [[noise_source1, bio_source]],
                   (LINES[0], 41): [[noise_source1, bio_source]],
                   (LINES[0], 42): [[noise_source1, bio_source]],
                   (LINES[0], 43): [[noise_source2, bio_source]],
                   (LINES[0], 44): [[noise_source2, bio_source]],
                   (LINES[0], 45): [[noise_source2, bio_source]],
                   (LINES[0], 46): [[noise_source3, bio_source]],
                   (LINES[0], 47): [[noise_source3, bio_source]],
                   (LINES[0], 48): [[noise_source3, bio_source]],
                   (LINES[0], 49): [[noise_source4, bio_source]],
                   (LINES[0], 50): [[noise_source4, bio_source]],
                   (LINES[0], 51): [[noise_source4, bio_source]],
                   }

    # no changes needed
    # extracts list of sources from dict
    sources = []

    for key in source_dict:
        for i in range(len(source_dict[key])):
            sources = sources + source_dict[key][i]

    sources = list(set(sources))

    return sources, source_dict
